#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <string.h>

/* Programme client */

int main(int argc, char *argv[])
{

   if (argc != 4)
   {
      printf("utilisation : %s ip_serveur port_serveur itération\n", argv[0]);
      printf("localhost : 127.0.0.1");
      exit(1);
   }

   int iterations = atoi(argv[3]);

   /* Etape 1 : créer une socket */
   int descripteurSocketClient = socket(PF_INET, SOCK_STREAM, 0);

   if (descripteurSocketClient == -1)
   {
      perror("Client : problème de création de la socket :");
      exit(1);
   }
   printf("Client : creation de la socket réussie \n");

   /* Etape 2 : Nommer la socket du client */
   struct sockaddr_in adresseServeur;
   adresseServeur.sin_family = AF_INET;
   inet_pton(AF_INET, argv[1], &(adresseServeur.sin_addr));
   adresseServeur.sin_port = htons((short)atoi(argv[2]));
   socklen_t longueurAdresse = sizeof(struct sockaddr_in);

   /* Etape 3 : Demande de connexion */

   int res = connect(descripteurSocketClient, (struct sockaddr *)&adresseServeur, longueurAdresse);

   if (res == -1)
   {
      perror("Client : Problème de connexion");
      exit(1);
   }

   printf("Client : connexion réussis \n");

   /* Input du message */

   char *message[1500];
   printf("Saisir un message : \n >>");
   scanf("%s", message);
   printf("Client : le client envoie _n | %s %d fois \n", message, iterations);

   /* Etape 4 : envoyer un message au serveur  (voir sujet pour plus de détails)*/

   char sizemessage[10];
   printf(sizemessage, "%d", strlen(message));

   int octetsTotaux = 0;
   for (int i = 1; i <= iterations; i++)
   {
      if (i == 1)
      {
         printf("Client : %der envoier \n", i);
      }
      else
      {
         printf("Client : %dième envoie \n", i);
      }

      // Envoie de la taille du message
     int tailleMessage = 0;
     tailleMessage += send(descripteurSocketClient,
                            sizemessage,
                            strlen(sizemessage) + 1,
                            0);

      // Attente de confirmation

      recv(descripteurSocketClient,
           sizemessage,
           sizeof(sizemessage),
           0);

      // Envoie du message

      res = send(descripteurSocketClient,
                 message,
                 strlen(message) + 1,
                 0);
      tailleMessage += res;

      printf("Client : message envoyé. Taille : %d octets \n", res);
      printf("Client : %d octets envoyés, %d appels a send() \n",tailleMessage,iterations*2);
     
   }

   

   /* Etape 6 : fermer la socket (lorsqu'elle n'est plus utilisée)*/

   close(descripteurSocketClient);

   printf("Client : je termine\n");
   return 0;
}
