#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <string.h>

/* Programme serveur */

int main(int argc, char *argv[])
{

   if (argc != 2)
   {
      printf("utilisation : %s port_serveur\n", argv[0]);
      exit(1);
   }

   /* Etape 1 : créer une socket */
   int descripteurServeur = socket(PF_INET, SOCK_STREAM, 0);

   if (descripteurServeur == -1)
   {
      perror("Serveur : problème de creation socket :");
      exit(1);
   }

   printf("Serveur : creation de la socket réussie \n");

   /* Etape 2 : Nommer la socket du seveur */

   struct sockaddr_in adresseServeur;
   adresseServeur.sin_family = AF_INET;
   adresseServeur.sin_addr.s_addr = INADDR_ANY;
   adresseServeur.sin_port = htons((short)atoi(argv[1]));
   socklen_t longueurAdresse = sizeof(struct sockaddr_in);

   int res = bind(descripteurServeur,
                  (struct sockaddr *)&adresseServeur,
                  sizeof(adresseServeur));

   if (res == -1)
   {
      perror("Serveur : Problème de création de la socket :");
      exit(1);
   }

   printf("Serveur : création de la socket reussie \n");

   /*Etape 3 : Listen de la socket */

   res = listen(descripteurServeur, 10);

   if (res == -1)
   {
      perror("Serveur : problème d'écoute de la socket");
      exit(1);
   }

   printf("Serveur : attente d'un message du client... \n");

   /* Etape 4 : recevoir un message du client (voir sujet pour plus de détails)*/

   struct sockaddr_in socketClient;

   int descripteurClient = accept(descripteurServeur,
                                  (struct sockaddr *)&socketClient,
                                  &longueurAdresse);

   if (descripteurClient == -1)
   {
      perror("Serveur : problème d'accept de la socket");
      exit(1);
   }

   printf("Serveur : socket accepter\n");

   int tailleTotal = 0;
   int count = 0;

   while (1)
   {

      // Réception de la taille du message
      char nbOctet[10];
      tailleTotal += recv(descripteurClient,
                          nbOctet,
                          sizeof(nbOctet),
                          0);

      if (tailleTotal == -1)
   {
      perror("Serveur : problème de réception de la taille du message");
      exit(1);
   }

   printf("Serveur : taille du message reçu \n");

      // Envoie de confirmation

      send(descripteurClient,
           &nbOctet,
           strlen(nbOctet) + 1,
           0);

      // Réception du message

      char message[atoi(nbOctet)];
      tailleTotal += recv(descripteurClient,
                          message,
                          sizeof(message),
                          0);

      count++;

      printf("Serveur : %d octets reçus, %d appels à recv() \n", tailleTotal, count * 2);
      
   }
   /* Etape 6 : fermer la socket (lorsqu'elle n'est plus utilisée)*/

   printf("Serveur : je termine\n");
   return 0;
}
