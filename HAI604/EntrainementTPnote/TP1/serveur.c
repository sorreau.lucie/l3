#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <string.h>

/* Programme serveur */

int main(int argc, char *argv[])
{

   if (argc != 2)
   {
      printf("utilisation : %s port_serveur\n", argv[0]);
      exit(1);
   }

   /* Etape 1 : créer une socket */
   int descripteurServeur = socket(PF_INET, SOCK_DGRAM, 0);

   if (descripteurServeur == -1)
   {
      perror("Serveur : problème de creation de socket :");
      exit(1);
   }

   printf("Serveur : creation de la socket réussie \n");

   // Je peux tester l'exécution de cette étape avant de passer à la
   // suite. Faire de même pour la suite : n'attendez pas de tout faire
   // avant de tester.

   /* Etape 2 : Nommer la socket du seveur */

   struct sockaddr_in sockServeur;
   sockServeur.sin_family = AF_INET;
   sockServeur.sin_addr.s_addr = INADDR_ANY;
   sockServeur.sin_port = htons((short)atoi(argv[1]));

   int res = bind(descripteurServeur,              // descripteur du serveur
                  (struct sockaddr *)&sockServeur, // pointeur vers l'adresse serveur
                  sizeof(sockServeur));            // longueur de l'adresse

   if (res == -1)
   {
      perror("serveur : problème de bind sur la socket serveur");
      exit(1);
   }

   printf("Serveur : bind de la socket reussis \n");
   printf("Serveur :attente d'un message du client ...\n");

   /* Etape 4 : recevoir un message du client (voir sujet pour plus de détails)*/

   while(1){
    struct sockaddr_in socketClient;
    socklen_t longueurAdresse;

    char message [200];
    res = recvfrom(descripteurServeur, &message, sizeof(message), 0,(struct sockaddr*) &socketClient, &longueurAdresse);

    printf("\nServeur : j'ai reçu \n | %s", message);
    printf("\n envoyé par : %d\n", ntohs(socketClient.sin_port));
    printf("\n Taille du message: %d \n", res);
   }

   /* Etape 6 : fermer la socket (lorsqu'elle n'est plus utilisée)*/

   close(descripteurServeur);

   printf("Serveur : je termine\n");
   return 0;
}
