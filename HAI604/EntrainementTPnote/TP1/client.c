#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <string.h>

/* Programme client */

int main(int argc, char *argv[])
{

   /* je passe en paramètre l'adresse de la socket du serveur (IP et
      numéro de port) et un numéro de port à donner à la socket créée plus loin.*/

   if (argc != 4)
   {
      printf("utilisation : %s ip_serveur port_serveur port_client\n", argv[0]);
      exit(1);
   }

   /* Etape 1 : créer une socket */
   int descripteursockClient = socket(PF_INET, SOCK_DGRAM, 0);

   if (descripteursockClient == -1)
   {
      perror("Client : pb creation socket :");
      exit(1);
   }

   /* J'ajoute des traces pour comprendre l'exécution et savoir
      localiser des éventuelles erreurs */
   printf("Client : creation de la socket réussie \n");

   // Je peux tester l'exécution de cette étape avant de passer à la
   // suite. Faire de même pour la suite : n'attendez pas de tout faire
   // avant de tester.

   /* Etape 2 : Nommer la socket du client */
   struct sockaddr_in SockClient;
   SockClient.sin_family = AF_INET;
   SockClient.sin_addr.s_addr = INADDR_ANY;
   SockClient.sin_port = htons((short)atoi(argv[3])); // recupère le port client

   int res = bind(descripteursockClient, (struct sockaddr *)&SockClient, sizeof(SockClient)); // descripteur, pointeur vers l'adresse, longueur de l'adresse

   if (res == -1)
   {
      perror("client : problème lors du bind de la socket du client");
      exit(1);
   }

   printf("client: bind de la socket du client reussis \n");

   /* Etape 3 : Désigner la socket du serveur */

   struct sockaddr_in sockServeur;
   sockServeur.sin_family = AF_INET;
   sockServeur.sin_addr.s_addr = inet_addr(argv[1]);   // recupère l'ip du serveur
   sockServeur.sin_port = htons((short)atoi(argv[2])); // récupère le port du serveur

   /* Etape 4 : envoyer un message au serveur  (voir sujet pour plus de détails)*/

   char message[200];
   printf("Veuillez saisir un message : ");
   scanf("%[^\n]", message);
   printf("Client : envoie du message");

   socklen_t longueurAdresse = sizeof(struct sockaddr_in);

   res = sendto(descripteursockClient, // descripteur de la socket
                &message, // pointeur vers le 1er octet du message
                strlen(message) + 1, // taille du message
                0, // Option d'envoie
                (struct sockaddr *)&sockServeur, // pointeur vers l'adresse de la socket du serveur
                longueurAdresse); // longueur de l'adresse

   if (res == -1)
   {
      perror("client : pb envoie message");
      exit(1);
   }
   printf("message envoye");

   /* Etape 6 : fermer la socket (lorsqu'elle n'est plus utilisée)*/

   close(descripteursockClient);

   printf("Client : je termine\n");
   return 0;
}
