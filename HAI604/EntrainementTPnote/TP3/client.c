#include <stdio.h> 
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <string.h>
#include <sys/stat.h>

#define MAX_BUFFER_SIZE 146980

int main(int argc, char *argv[]) {

  /* etape 0 : utilisation */
  if (argc != 4){
    printf("Utilisation : client ip_serveur port_serveur nom_fichier\n Remaque : le parametre nom_fichier correspond au nom d'un fichier existant dans le répertoire emission\n");
    exit(0);
  }

  /* etape 1 : créer une socket */
  int descripteur_socket = socket(PF_INET, SOCK_STREAM, 0);
  if (descripteur_socket == -1) {
    perror("Création d'une socket : PB.");
    close(descripteur_socket);
    exit(1);
  }
  printf("Création d'une socket : OK.\n");
  
  /* etape 2 : designer la socket du serveur */
  struct sockaddr_in adresse_serveur;
  adresse_serveur.sin_family = AF_INET;
  adresse_serveur.sin_port = htons((short)atoi(argv[2]));
  int resultat = inet_pton(AF_INET, argv[1], &(adresse_serveur.sin_addr));
  if (resultat == -1) {
    perror("Désignation de la socket du serveur : PB.\n");
    close(descripteur_socket);
    exit(1);
  }
  printf("Désignation de la socket du serveur : OK.\n");
 
  /* etape 3 : demander une connexion */
  resultat = connect(descripteur_socket, (struct sockaddr *) &adresse_serveur, sizeof(adresse_serveur));
  if (resultat == -1) {
    printf("Demande d'une connexion : PB.\n");
    close(descripteur_socket);
    exit(1);
  }
  printf("Demande d'une connexion : OK.\n");
 
  /* etape 4 : envoi de fichier */
  // construction du nom du chemin vers le fichier
  char* filepath = malloc(strlen(argv[3]) + 16); // ./emission/+nom fichier +\0
  filepath[0] = '\0';
  strcat(filepath, "./emission/");
  strcat(filepath, argv[3]);
  printf("Fichier à envoyer : %s\n", filepath);

  // obtenir la taille du fichier
  struct stat attributes;
  if(stat(filepath, &attributes) == -1){
    perror("Client : erreur stat");
    free(filepath);
    exit(1);
  }
  long long int file_size = attributes.st_size; // cette copie est uniquement informer d'où obtenir la taille du fichier.
  printf("Taille du fichier (en octets) : %lld\n", file_size);

  // envoi de la taille du fichier
  resultat = send(descripteur_socket, &file_size, sizeof(file_size), 0);
  if (resultat <= 0) {
    printf("Envoi de la taille du fichier : PB.\n");
    close(descripteur_socket);
    exit(1);
  }
  printf("Envoi de la taille du fichier : OK.\n");

  // envoi du nom du fichier
  resultat = send(descripteur_socket, argv[3], strlen(argv[3]) + 1, 0);
  if (resultat <= 0) {
    printf("Envoi du nom du fichier : PB.\n");
    close(descripteur_socket);
    exit(1);
  }
  printf("Envoi du nom du fichier : OK.\n");
  
  // ouverture du fichier
  FILE* file = fopen(filepath, "rb");
  if(file == NULL){
    perror("Client : erreur ouverture fichier \n");
    free(filepath);
    exit(1);   
  }
  free(filepath);

  // lecture des blocs et envoi des blocs
  long long int octets_lus = 0;
  long long int octets_deposes = 0;
  char buffer[MAX_BUFFER_SIZE];

  while(octets_lus < file_size){
    size_t read = fread(buffer, sizeof(char), MAX_BUFFER_SIZE, file);
    if(read == 0){
      if(ferror(file) != 0){
	perror("Lecture d'un bloc du fichier : PB \n");
	fclose(file);
	exit(1);
      }else{
        printf("Arrivé a la fin du la lecture du fichier\n");// fin du fichier
	break;
      }
    }
    printf("Taille du bloc lu : %ld\n", read);  
    octets_lus += read;
    
    // envoi d'un bloc du fichier
    resultat = send(descripteur_socket, buffer, read, 0);
    if (resultat <= 0) {
      printf("Envoi d'un bloc du fichier : PB.\n");
      close(descripteur_socket);
      exit(1);
    }
    printf("Taille du bloc envoyé : %d\n", resultat);
    octets_deposes += resultat;
  }

  /* fermeture du fichier */
  fclose(file); 
   
  printf("Octets lus : %lld\n", octets_lus);
  printf("Octets déposés : %lld\n", octets_deposes); 
 
  printf("Fermeture : OK.\n");
}
