#include <stdio.h> 
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <string.h>
#include <errno.h>


int sendTCP(int sock, void* msg, int sizeMsg) {
    int res;
    int sent=0;
    //boucle d'envoi
    while(sent < sizeMsg) {
        //printf("Reste à envoyer: %i (res %i) envoyé %i\n",sizeMsg-sent, res, sent);
        res = send(sock, msg+sent, sizeMsg-sent, 0);
        sent += res;
        if (res == -1) {
            printf("Problème lors de l'envoi du message\n");
            printf("errno: %d , %s\n", errno, strerror(errno));
            return -1;
        }
    }
    return 1;
}

int recvTCP(int sock, void* msg, int sizeMsg) {
    int res;
    int received=0;
    //boucle de réception
    while(received < sizeMsg) {
        res = recv(sock, msg+received, sizeMsg-received, 0);
        received += res;
        if (res == -1) {
            printf("Problème lors de la réception du message\n");
            printf("errno: %d , %s\n", errno, strerror(errno));
            return -1;
        } else if (res == 0) {
            return 0;
        }
    }
    return 1;
}
