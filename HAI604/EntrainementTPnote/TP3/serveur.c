#include <stdio.h>//perror
#include <sys/types.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <unistd.h>//close
#include <stdlib.h>
#include <string.h>


#define MAX_BUFFER_SIZE 146980

int main(int argc, char *argv[]) {

  /* etape 0 : gestion des paramètres si vous souhaitez en passer */
  if (argc != 2) {
    printf("utilisation : %s port_serveur\n", argv[0]);
    exit(1);
  }
  
  /* etape 1 : creer une socket d'écoute des demandes de connexions*/
  int descripteur_socket = socket(PF_INET, SOCK_STREAM, 0);
  if (descripteur_socket == -1) {
    perror("Création d'une socket d'écoute des demandes de connexions : PB.\n");
    exit(1);
  }
  printf("Création d'une socket d'écoute des demandes de connexions : OK.\n");
  
  /* etape 2 : nommage de la socket */
  struct sockaddr_in adresse_serveur;
  adresse_serveur.sin_family = AF_INET;
  adresse_serveur.sin_port = htons((short)atoi(argv[1]));
  adresse_serveur.sin_addr.s_addr = INADDR_ANY;
  int resultat = bind(descripteur_socket, (struct sockaddr*)&adresse_serveur, sizeof(adresse_serveur));
  if (resultat == -1) {
     perror("Nommage de la socket : PB.\n");
     close(descripteur_socket);
     exit(1);
  }
  printf("Nommage de la socket : OK.\n");
  
  /* etape 3 : mise en ecoute des demandes de connexions */
  resultat = listen(descripteur_socket, 10);
  if (resultat == -1) {
    perror("Mise en ecoute des demandes de connexions : PB.\n");
    close(descripteur_socket);
    exit(1);
  }
  printf("Mise en ecoute des demandes de connexions : OK.\n");
  
  
  while(1){
  /* etape 4 : plus qu'a attendre la demande d'un client */
  struct sockaddr_in adresse_client;
  socklen_t longueur_adresse_client = sizeof(adresse_client);
  int descripteur_socket_client = accept(descripteur_socket, (struct sockaddr *) &adresse_client, &longueur_adresse_client);
  
  
  if (descripteur_socket_client == -1) {
    perror("Acceptation de la demande de connexion : PB.\n");
    close(descripteur_socket);
    close(descripteur_socket_client);
    exit(1);
  }
  printf("Acceptation de la demande de connexion : OK.\n");
	if(fork() == 0){
  // réception de la taille du fichier
  long long int taille_fichier = 0;
  resultat = recv(descripteur_socket_client, &taille_fichier, sizeof(taille_fichier), 0);
    if (resultat <= 0) {
      perror("Réception de la taille du fichier : PB.\n");
      close(descripteur_socket);
      exit(1);
  }
  printf("Taille du fichier : %lld\n", taille_fichier);
 
  // réception du nom du fichier
  char nom_fichier[4000];
  resultat = recv(descripteur_socket_client, nom_fichier, sizeof(nom_fichier), 0);
    if (resultat == -1) {
      perror("Réception du nom du fichier : PB.\n");
      close(descripteur_socket);
      exit(1);
  }
  printf("Nom du fichier : %s\n", nom_fichier);

  // création du fichier
  char* filepath = malloc(17 + taille_fichier);
  filepath[0] = '\0';
  strcat(filepath, "./reception/");
  strcat(filepath, nom_fichier);
   
  // On ouvre le fichier dans lequel on va écrire
  FILE* file = fopen(filepath, "wb");
  if(file == NULL){
    perror("Ouverture du fichier : PB.\n");
    exit(1);  
  }

  // réception des blocs du fichier
  char buffer[MAX_BUFFER_SIZE];
  long long int octets_extraits = 0;
  long long int octets_ecrits = 0;
  while (octets_extraits < taille_fichier) {
    resultat = recv(descripteur_socket_client, buffer, MAX_BUFFER_SIZE, 0);
    octets_extraits += resultat;
      if (resultat == -1) {
        perror("Réception d'un bloc du fichier : PB.\n");
        close(descripteur_socket);
        exit(1);
      }
    printf("Taille du bloc extrait : %d\n", resultat);
    size_t written = fwrite(buffer, sizeof(char), (resultat < (taille_fichier - octets_ecrits) ? resultat : (taille_fichier - octets_ecrits)), file);
    printf("Taille du bloc écrit : %ld\n", written);
    octets_ecrits += written;
  }
  printf("Octets extraits au total : %lld\n", octets_extraits),
  printf("Octets écrits au total : %lld\n", octets_ecrits);
  printf("Ecriture du fichier : OK.\n");
  
  /* Fermeture */
  fclose(file);
    
  printf("Fermeture : Fermeture du fichier.\n");
  free(filepath);
  printf("Fermeture : Libération poiteur fichier (filePath).\n");
  
    close(descripteur_socket_client);
  }else{
    close(descripteur_socket_client);
  }
  printf("Fermeture de la socket client. \n");
  
  
  
  
}

  close(descripteur_socket);
  
    printf("Fermeture de la socket serveur. \n");
  }










