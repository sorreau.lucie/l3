#include <stdio.h> 
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdlib.h>
#include<arpa/inet.h>
#include<string.h>

/* Programme client */

int main(int argc, char *argv[]) {

  /* je passe en paramètre l'adresse de la socket du serveur (IP et
     numéro de port) et un numéro de port à donner à la socket créée plus loin.*/

  /* Je teste le passage de parametres. Le nombre et la nature des
     paramètres sont à adapter en fonction des besoins. Sans ces
     paramètres, l'exécution doit être arrétée, autrement, elle
     aboutira à des erreurs.*/
  if (argc != 4){
    printf("utilisation : %s ip_serveur port_serveur port_client\n", argv[0]);
    char* IPserveur = argv[1];
    char* Pserveur = argv[2];
    char* Pclient = argv[3];

    exit(1);
  }

  /* Etape 1 : créer une socket */   
  int ds = socket(PF_INET, SOCK_DGRAM, 0);


  if (ds == -1){
    perror("Client : pb creation socket :");
    exit(1);
  }
  
 
  printf("Client : creation de la socket réussie \n");
  
  // Je peux tester l'exécution de cette étape avant de passer à la
  // suite. Faire de même pour la suite : n'attendez pas de tout faire
  // avant de tester.
  
  /* Etape 2 : Nommer la socket du client */
struct sockaddr_in ad ;
ad.sin_family = AF_INET ;
ad.sin_addr.s_addr = INADDR_ANY ;
ad.sin_port = htons( (short) atoi(argv[3])); 
int res = bind(ds, (struct sockaddr*)&ad, sizeof(ad)) ;

if (res==-1){
   perror("client : pb bind socket");
   exit(1);
}

printf("client: bind socke ok \n");

  
  /* Etape 3 : Désigner la socket du serveur */
struct sockaddr_in sockDistante ;
sockDistante.sin_family = AF_INET ;
sockDistante.sin_addr.s_addr = inet_addr(argv[1]); 
sockDistante.sin_port = htons( (short) atoi(argv[2])) ; 



  
  /* Etape 4 : envoyer un message au serveur  (voir sujet pour plus de détails)*/

char message[100];
printf("  entrez votre message: ");
scanf ("%[^\n]",message);
printf("client : envoie du message ");

socklen_t lgAdr = sizeof(struct sockaddr_in);

res =sendto(ds,&message,strlen(message)+1,0,(struct sockaddr *)&sockDistante,lgAdr);
 
 if (res==-1){
   perror ("client : pb envoie message");
   exit(1);
 }
  printf("message envoye");
  /* Etape 5 : recevoir un message du serveur (voir sujet pour plus de détails)*/


  
  /* Etape 6 : fermer la socket (lorsqu'elle n'est plus utilisée)*/
  close(ds);
  
  printf("Client : je termine\n");
  return 0;
}


//ip localhost 127.0.0.1
//port serveur 51510
//port client 61510