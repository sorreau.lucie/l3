#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdio.h>//perror
#include <unistd.h>
#include <string.h>

// une base pour commencer à mettre en place des arborescences de processus avec fork()

int largeurParam(int largeur, int parent) {
    while (largeur > 0 ) {
        if (parent == getpid()) {
            int resFork = fork();
            if (resFork == -1) {
                perror("Erreur fork : "); 
                exit(1);
            }
            largeur--;
        }
        if (parent != getpid()) return 1;
    }
    return 1;
}
int hauteurParam(int hauteur, int parent) {
    int resFork = fork();
    if (resFork == -1) {
        perror("Erreur fork : ");
        exit(1);
    }
    if (parent == getpid()) return parent;
    if (hauteur > 1) {
        return hauteurParam(hauteur-1, getpid());
    }
    return parent;
}
int binaireParam(int hauteur, int parent) {
    largeurParam(2, parent);
    if (parent == getpid()) return parent;
    if (hauteur > 1) {
        return binaireParam(hauteur-1, getpid());
    }
    return parent;
}

int  main (int argc, char *argv[]){

    if (argc < 3) {
        printf ("Utilisation : %s type_lancement nombre_processus\n", argv[0]);
        exit(1);
    }

    int nbProc = atoi(argv[2]);



    //CALER LES PIPES AVANT LE FORK ET RECOMMENCER AVANT CHAQUE FORK? 

    int fd1[2]; // Used to store two ends of first pipe
    int fd2[2];
    char fixed_str[] = "testouillage";char input_str[100];

    if (pipe(fd1) == -1) {
        fprintf(stderr, "Pipe Failed");
        return 1;
    }
    if (pipe(fd2) == -1) {
        fprintf(stderr, "Pipe Failed");
        return 1;
    }
    scanf("%s", input_str);

    int parent = getpid();
    printf("Avant fork : pid parent racine : %d \n", parent);

    if (nbProc > 100) nbProc = 100;

    if (argv[1][0] == '1') largeurParam(nbProc, parent);
    if (argv[1][0] == '2') {
        parent = hauteurParam(nbProc, parent);
    }
    if (argv[1][0] == '3') {
        if (nbProc > 5) nbProc = 5;
        parent = binaireParam(nbProc, parent);
    }

    if (parent == getpid()){ // je suis dans le processus racine
        printf("Après fork : je suis le processus : %d et je commence mon travail\n", parent);
        // je fait une simulation d'un calcul en parralèle avec le fils


        char concat_str[100];
        close(fd1[0]); // Close reading end of first pipe
        // Write input string and close writing end of first
        // pipe.
        write(fd1[1], input_str, strlen(input_str) + 1);
        close(fd1[1]);

        wait(NULL);
        close(fd2[1]); // Close writing end of second pipe
 
        // Read string from child, print it and close
        // reading end.
        read(fd2[0], concat_str, 100);
        printf("Concatenated string %s\n", concat_str);
        close(fd2[0]);


        sleep (5); // s'endormir pendant 5 secondes     	
    }
    
    if (parent != getpid()){
        printf("Après fork : je suis le processus : %d (fils de %i) et je commence mon travail \n", getpid(), parent);

        close(fd1[1]); // Close writing end of first pip
        // Read a string using first pipe
        char concat_str[100];
        read(fd1[0], concat_str, 100);
        printf("Je suis le processus: %d et j'ai reçu le message: %s\n",getpid(),concat_str);
        // Concatenate a fixed string with it
        int k = strlen(concat_str);
        int i;
        for (i = 0; i < strlen(fixed_str); i++)
            concat_str[k++] = fixed_str[i];
        concat_str[k] = '\0'; // string ends with '\0'
 
        // Close both reading ends
        close(fd1[0]);
        close(fd2[0]);
 
        // Write concatenated string and close writing end
        write(fd2[1], concat_str, strlen(concat_str) + 1);
        close(fd2[1]);











        sleep(10);
        printf("Je suis le processus : %d et j'ai terminé mon travail\n", getpid());
        exit(0);
    }
    
    // la suite ici n'est faite que par le processus racine
    printf("Je suis le processus : %d. J'ai terminé mon travail et doit attendre la fin du processus fils que j'ai créé\n", getpid());  
    
    while(wait(0)!=-1);  // cette instruction permet d'attendre la fin de l'execution de tous mes fils.
    printf("Je suis le processus : %d, mon fils s'est terminé, je peux terminer à mon tour\n", getpid()); 
    
    return 0;
}







