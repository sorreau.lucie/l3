#include <stdio.h> 
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <string.h>
#include <errno.h>

/* Programme serveur */

int main(int argc, char *argv[]) {

   /* Je passe en paramètre le numéro de port qui sera donné à la socket créée plus loin.*/

   /* Je teste le passage de parametres. Le nombre et la nature des
      paramètres sont à adapter en fonction des besoins. Sans ces
      paramètres, l'exécution doit être arrétée, autrement, elle
      aboutira à des erreurs.*/
   if (argc != 2){
      printf("utilisation : %s port_serveur\n", argv[0]);
      exit(1);
   }

   /* Etape 1 : créer une socket */   
   int ds = socket(PF_INET, SOCK_DGRAM, 0);

   /* /!\ : Il est indispensable de tester les valeurs de retour de
      toutes les fonctions et agir en fonction des valeurs
      possibles. Voici un exemple */
   if (ds == -1){
      perror("Serveur : pb creation socket :");
      exit(1); // je choisis ici d'arrêter le programme car le reste
         // dépendent de la réussite de la création de la socket.
   }
   
   /* J'ajoute des traces pour comprendre l'exécution et savoir
      localiser des éventuelles erreurs */
   printf("Serveur : creation de la socket réussie \n");
   
   // Je peux tester l'exécution de cette étape avant de passer à la
   // suite. Faire de même pour la suite : n'attendez pas de tout faire
   // avant de tester.
   
   /* Etape 2 : Nommer la socket du seveur */
   struct sockaddr_in ad;
   ad.sin_family = AF_INET;
   ad.sin_addr.s_addr = htonl(INADDR_ANY);  //inet_addr("91.168.70.48");
   ad.sin_port = htons((short) atoi(argv[1]));
   int res = bind(ds, (struct sockaddr*)&ad, sizeof(ad));
   if (res == 0) {
      printf("Socket nommée avec succès\n");
      printf("port: %i\n",ntohs(ad.sin_port));
      // char *ip = inet_ntoa(ad.sin_addr);
      // printf("ip: %s\n", ip); //0.0.0.0 signifie qu'on accepte tout ce qui rentre
   } else {
      printf("Socket non nommée : %i \n", res);
      printf("errno: %d , %s\n", errno, strerror(errno));
      exit(1);
   }

   /* Etape 4 : recevoir un message du client (voir sujet pour plus de détails)*/
   struct sockaddr_in sockClient;
   socklen_t lgAdr;
   char message[200];
   char str[INET_ADDRSTRLEN];
   char *msg = "Merci pour ton message!";
   res=-1;
   while (1) {
      res=recvfrom(ds, message, sizeof(message), 0, (struct sockaddr*)&sockClient, &lgAdr);
      inet_ntop(AF_INET, &sockClient.sin_addr, str, INET_ADDRSTRLEN);
      printf("message de longueur %i reçu de l'adresse %s\nMessage: %s\n",res,str,message);
      res = sendto(ds, msg, strlen(msg)+1, 0, (struct sockaddr *)&sockClient, lgAdr);

   }
   /* Etape 5 : envoyer un message au serveur (voir sujet pour plus de détails)*/
   
   /* Etape 6 : fermer la socket (lorsqu'elle n'est plus utilisée)*/
   
   
   printf("Serveur : je termine\n");
   return 0;
}
