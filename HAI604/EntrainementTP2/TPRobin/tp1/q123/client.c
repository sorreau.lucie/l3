#include <stdio.h> 
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <string.h>
#include <errno.h>

/* Programme client */

//serveur fac : 91.174.102.81:32768

extern int errno;

int main(int argc, char *argv[]) {

   /* je passe en paramètre l'adresse de la socket du serveur (IP et
      numéro de port) et un numéro de port à donner à la socket créée plus loin.*/
   if (argc != 4){
      printf("utilisation : %s ip_serveur port_serveur port_client\n", argv[0]);
      exit(1);
   }

   /* Etape 1 : créer une socket */   
   int ds = socket(PF_INET, SOCK_DGRAM, 0);

   /* /!\ : Il est indispensable de tester les valeurs de retour de
      toutes les fonctions et agir en fonction des valeurs
      possibles. Voici un exemple */
   if (ds == -1){
      perror("Client : pb creation socket :");
      exit(1); // je choisis ici d'arrêter le programme car le reste
         // dépendent de la réussite de la création de la socket.
   }
   
   /* J'ajoute des traces pour comprendre l'exécution et savoir
      localiser des éventuelles erreurs */
   printf("Client : creation de la socket réussie \n");
   
   /* Etape 2 : Nommer la socket du client */
   /*
   struct sockaddr {
      sa_family_t sa_family;
      char sa_data[14];
   };
   struct sockaddr_in {
      sa_family_t sin_family;
      in_port_t sin_port;
      struct in_addr sin_addr;
   };
   */
   struct sockaddr_in ad;
   ad.sin_family = AF_INET;
   ad.sin_addr.s_addr = INADDR_ANY;
   ad.sin_port = htons((short) atoi(argv[3]));
   int res = bind(ds, (struct sockaddr*)&ad, sizeof(ad));
   if (res == 0) {
      printf("Socket nommée avec succès\n");
   } else {
      printf("Socket non nommée : %i \n", res);
      printf("errno: %d , %s\n", errno, strerror(errno));
      exit(1);
   }

   socklen_t size = sizeof(ad);
   getsockname(ds, (struct sockaddr*) &ad, &size);
   printf("%s:%i\n", inet_ntoa(ad.sin_addr), ntohs((short) ad.sin_port));

   /* Etape 3 : Désigner la socket du serveur */
   struct sockaddr_in sockServ;
   sockServ.sin_family = AF_INET;

   /*
   struct hostent *he;
   he = gethostbyname("0.0.0.0");
   if (he == NULL) {
      printf("bouuuf");
   } else {
      printf("foundit\n");
   }
   */

   // sockServ.sin_addr.s_addr = inet_addr(argv[1]);
   sockServ.sin_addr.s_addr = inet_addr(argv[1]);
   sockServ.sin_port = htons((short)atoi(argv[2]));
   socklen_t lgAdr = sizeof(struct sockaddr_in);
   /* Etape 4 : envoyer un message au serveur  (voir sujet pour plus de détails)*/
   //char* msg = "Bonjour c'est Robin.";
   char msg[200];
   printf("Envoyer un message: ");
   fgets(msg, sizeof(msg), stdin);

   ssize_t res2 = sendto(ds, &msg, strlen(msg)+1, 0, (struct sockaddr *)&sockServ, lgAdr);
   printf("\nMessage \"%s\" envoyé à l'adresse %s / port %i / longueur %li\n",msg,inet_ntoa(sockServ.sin_addr),ntohs(sockServ.sin_port),res2);
   // printf("%i, %s  \n",sockServ.sin_port, argv[2]);
   /* Etape 5 : recevoir un message du serveur (voir sujet pour plus de détails)*/
   char message[200];
   char str[INET_ADDRSTRLEN];
   res=-1;
   while (1) {
      res=recvfrom(ds, message, sizeof(message), 0, (struct sockaddr*)&sockServ, &lgAdr);
      inet_ntop(AF_INET, &sockServ.sin_addr, str, INET_ADDRSTRLEN);
      printf("message de longueur %i reçu de l'adresse %s\nMessage: %s\n",res,str,message);
   }
   /* Etape 6 : fermer la socket (lorsqu'elle n'est plus utilisée)*/
   
   
   printf("Client : je termine\n");
   return 0;
}
