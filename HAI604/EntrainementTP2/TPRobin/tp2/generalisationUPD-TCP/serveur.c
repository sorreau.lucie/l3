#include <stdio.h> 
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <string.h>
#include <errno.h>

/* Programme serveur */

extern int errno;

int main(int argc, char *argv[]) {

   if (argc != 2){
      printf("utilisation : %s port_serveur\n", argv[0]);
      exit(1);
   }

   /* Etape 1 : créer une socket */   
   int ds = socket(PF_INET, SOCK_STREAM, 0);
   if (ds == -1){
      perror("Serveur : pb creation socket :");
      exit(1); 
   }
   printf("Serveur : creation de la socket réussie \n");
   
   /* Etape 2 : Nommer la socket du serveur */
   struct sockaddr_in ad;
   ad.sin_family = AF_INET;
   ad.sin_addr.s_addr = htonl(INADDR_ANY);
   if (atoi(argv[1]) != -1) {
      ad.sin_port = htons((short) atoi(argv[1]));
   }
   int res = bind(ds, (struct sockaddr*)&ad, sizeof(ad));
   if (res == 0) {
      printf("Socket nommée avec succès\n");
      socklen_t sizeAd = sizeof(ad);
      getsockname(ds, (struct sockaddr*) &ad, &sizeAd);
      printf("port: %i\n",ntohs(ad.sin_port));
   } else {
      printf("Socket non nommée : %i \n", res);
      printf("errno: %d , %s\n", errno, strerror(errno));
      exit(1);
   }
   /* Etape 4: se connecter au client */
   struct sockaddr_in sockClient;
   socklen_t lgAdr;
   char str[INET_ADDRSTRLEN];
   char *msgRenvoi = "Merci pour ton message de longueur ";
   char buffer[1024];
   int sizeBuffer = 1024;
   int nbMaxAttente = 10;
   res=-1;
   
   //écouter
   int resListen = listen(ds, nbMaxAttente);
   if (resListen == -1) {
      printf("Problème lors de l'écoute\n");
      printf("errno: %d , %s\n", errno, strerror(errno));
      exit(1);    
   }
   //accepter une connexion
   int dsClient = accept(ds, (struct sockaddr*)&sockClient, &lgAdr);
   if (dsClient == -1) {
      printf("Problème lors de la connexion\n");
      printf("errno: %d , %s\n", errno, strerror(errno));
      exit(1);
   } else {
      inet_ntop(AF_INET, &sockClient.sin_addr, str, INET_ADDRSTRLEN);
      printf("Connexion à l'adresse %s",str);
   }

   /* Etape 4.5 : recevoir un message du client (voir sujet pour plus de détails)*/
   while (1) {
      
      //recevoir un message générique
      res = recv(dsClient, &buffer, sizeBuffer, 0);
      if (res == -1) {
         printf("Probleme de réception\n");
         printf("errno: %d , %s\n", errno, strerror(errno));
         close(dsClient);
         continue;
      }
      printf("--- Message reçu de longueur %i\n",res);
      //mettre ce message générique dans le paquet
      char type = buffer[0];
      printf("Type du paquet: %i\n", (int)type);
      //reconnaitre le type de données
      if(type == 1) {
         //message string
         char *msgStr = malloc(res);
         memcpy(msgStr, buffer+1, res-1);
         msgStr[res-1] = '\0';
         printf("Message: %s\n", msgStr);
         free(msgStr);
      } else if(type == 2) {
         //message tableau
         int* T = (int*)malloc(res-1);
         memcpy(T, buffer+1, res-1);
         printf("Tableau: [");
         for(int i=0; i < (int)((res-1)/sizeof(int)); i++) {
            printf(" %i ", T[i]);
         }
         printf("]\n");
         free(T);
      } else if(type == 3) {
         //message socket
         struct sockaddr_in *sock = (struct sockaddr_in*)malloc(sizeof(struct sockaddr_in));
         memcpy(sock, buffer+1, res-1);
         printf("Socket reçue contenant l'adresse %s et le port %i\n",inet_ntoa(sock->sin_addr),ntohs((short)sock->sin_port));
         free(sock);
      } else if(type == 4) {
         //message double
         double* T = (double*)malloc(res-1);
         memcpy(T, buffer+1, res-1);
         printf("Tableau: [");
         for(int i=0; i<50; i++) {
            printf(" %f ", T[i]);
         }
         printf("]\n");
         free(T);
      }
      //renvoyer un message
      char msgReponse[200];
      char buf[20];
      strcpy(msgReponse, msgRenvoi);
      snprintf(buf,sizeof(buf),"%i",res);
      strcat(msgReponse, buf);
      res = send(dsClient, msgReponse, strlen(msgReponse)+1, 0);
      if (res == -1) {
         printf("Problème lors de l'envoi de la réponse'\n");
         printf("errno: %d , %s\n", errno, strerror(errno));
         continue;
      }
   }
   //fermer le lien à la socket cliente
   printf("Connexion à l'adresse %s fermée. \n", str);
   close(dsClient);
   /* Etape 6 : fermer la socket (lorsqu'elle n'est plus utilisée)*/
   close(ds);
   printf("Serveur : je termine\n");
   return 0;
}
