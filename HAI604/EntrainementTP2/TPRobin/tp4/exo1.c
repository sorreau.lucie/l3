#include <string.h>
#include <stdio.h>//perror
#include <sys/types.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <pthread.h>
#include "calcul.h"


struct paramsFonctionThread {

    int idThread;
    int cpt;
    pthread_mutex_t mutex_cpt;
    // si d'autres paramètres, les ajouter ici.

};


void * fonctionThread (void * params){
    struct paramsFonctionThread * args = (struct paramsFonctionThread *) params;
    pthread_t moi = pthread_self();
    // a compléter
    printf("thread %li de param %i \n", moi, args->idThread);
    if (args->idThread == 2) {
        printf("Je fais un exit sauvage\n");
        exit(1);
    }
    pthread_mutex_lock(& (*args).mutex_cpt);
    for(int i=0; i<10000; i++) {
        args->cpt++;
    }
    pthread_mutex_unlock(& (*args).mutex_cpt);
    printf("fin thread %li , valeur de cpt: %i\n", moi, args->cpt);
    pthread_exit(NULL);
}

int main(int argc, char * argv[]){

    if (argc < 2 ){
        printf("utilisation: %s  nombre_threads  \n", argv[0]);
        return 1;
    }     

    
    pthread_t threads[atoi(argv[1])];


    struct paramsFonctionThread param;
    param.cpt = 0;
    pthread_mutex_init(& param.mutex_cpt, NULL);

    // création des threards 
    for (int i = 0; i < atoi(argv[1]); i++){

        // Le passage de paramètre est fortement conseillé (éviter les
        // variables globales).
        param.idThread = i;

        // compléter pour initialiser les paramètres
        if (pthread_create(&threads[i], NULL, fonctionThread, &param) != 0){
            perror("erreur creation thread");
            exit(1);
        }
    }


    // garder cette saisie et modifier le code en temps venu.
    char c[2]; 
    printf("saisir un caractère \n");
    fgets(c, 2, stdin);

    printf("caractère tapé: %c\n", c[0]);
    // ... compléter

    return 0;
 
}
 
