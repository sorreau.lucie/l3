#include <string.h>
#include <stdio.h>//perror
#include <sys/types.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <pthread.h>
#include "calcul.h"

void affiche(long unsigned int id, int num, int calcul, int cpt, int max) {
    printf("Thread %i (%li): calcul %i terminé (%i/%i)\n", num, id, calcul, cpt, max);
}

struct paramCommuns {
    int cpt;
    int max;
    pthread_mutex_t lock_cpt;
    pthread_cond_t cpt_rempli;
};

struct paramsFonctionThread {
    int idThread;
    struct paramCommuns *commun;
};

void * fonctionThread (void * params){
    struct paramsFonctionThread * args = (struct paramsFonctionThread *) params;
    pthread_t moi = pthread_self();
    struct paramCommuns *commun = args->commun;
    printf("hey\n");

    for(int i=0; i<10; i++) {
        calcul(1);
        pthread_mutex_lock(& commun->lock_cpt);
        commun->cpt++;
        affiche(moi,args->idThread,i,commun->cpt,commun->max);
        if (commun->cpt >= commun->max) {
            commun->cpt = 0;
            pthread_mutex_unlock(&commun->lock_cpt);
            pthread_cond_broadcast(&commun->cpt_rempli);
        } else {
            pthread_cond_wait(&commun->cpt_rempli, &commun->lock_cpt);
            pthread_mutex_unlock(&commun->lock_cpt);
        }
    }

    printf("fin thread %li\n", moi);
    pthread_exit(NULL);
}



int main(int argc, char * argv[]){

    if (argc < 2 ){
        printf("utilisation: %s  nombre_threads  \n", argv[0]);
        return 1;
    }     
    
    pthread_t threads[atoi(argv[1])];

    struct paramCommuns communs;
    communs.cpt = 0;
    communs.max = atoi(argv[1]);

    pthread_mutex_init(& communs.lock_cpt, NULL);
    pthread_cond_init(& communs.cpt_rempli, NULL);

    struct paramsFonctionThread param[atoi(argv[1])];
    //pthread_mutex_init(& param.mutex_cpt, NULL);
    // création des threads 
    for (int i = 0; i < atoi(argv[1]); i++){
        param[i].idThread = i;
        param[i].commun = &communs;
        // compléter pour initialiser les paramètres
        if (pthread_create(&threads[i], NULL, fonctionThread, &param[i]) != 0){
            perror("erreur creation thread");
            exit(1);
        }
    }


    // garder cette saisie et modifier le code en temps venu.
    char c[2]; 
    printf("saisir un caractère \n");
    fgets(c, 2, stdin);

    printf("caractère tapé: %c\n", c[0]);
    // ... compléter

    return 0;
 
}
 
