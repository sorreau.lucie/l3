#include <stdio.h> 
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <string.h>
#include <errno.h>
#include "tcp-utils.c"
#include <pthread.h>

/* Programme serveur */

extern int errno;


void* fonctionServeur(void* params) {
    struct paramCreationServeur * args = (struct paramCreationServeur *) params;
    int res;
    char buffer[50000];
    int nbAppelsRecv = 0;
    int tailleRecuDepuisDebut = 0;
    while(1) {
        //recevoir une taille de message à lire
        int taille;
        res = recvTCP(args->dsClient, &taille, sizeof(int));      
        //recevoir un message générique
        res = recvTCP(args->dsClient, buffer, taille);
        nbAppelsRecv++;
        if (res == -1) {
            printf("Probleme de réception\n");
            printf("errno: %d , %s\n", errno, strerror(errno));
            close(args->dsClient);
            continue;
        } else if (res == 0) {
            printf("Connexion fermée côté client\n");
            break;
        }
        //message string
        res = strlen(buffer)+1;
        printf("--- Message reçu de longueur %i\n",res);
        tailleRecuDepuisDebut += res;
        printf("Message: %s\n", buffer);
        printf("%i appels à recv (%i octets au total)\n", nbAppelsRecv, tailleRecuDepuisDebut);
    }
    close(args->dsClient);
    pthread_exit(NULL);
}

//void* fonctionServeurFTP(void* params) {
//    struct paramCreationServeur * args = (struct paramCreationServeur *) params;
//
//    int totalRecv = 0; // un compteur du nombre total d'octets recus d'un client
//    // recevoir le filepath
//    int taille = 0;
//    recvTCP(dsClient, &taille, sizeof(int));
//    char* tmpPath = (char*)malloc(taille);
//    char* filepath = (char*)malloc(taille + 15);
//    recvTCP(dsClient, tmpPath, taille);
//    filepath[0] = '\0';
//    strcat(filepath, "./reception/");
//    strcat(filepath, tmpPath);
//    free(tmpPath);
//    printf("Nom du fichier: %s\n", filepath);
//
//    // recevoir la taille du fichier
//    int file_size;
//    recvTCP(dsClient, &file_size, sizeof(int));
//    
//    // On ouvre le fichier dans lequel on va écrire
//    FILE* file = openFileFTP(filepath, "wb");
//
//    while(totalRecv < file_size) {
//        recvTCP(dsClient, &taille, sizeof(int));
//        totalRecv += recvTCP(dsClient, buffer, taille);
//        int written = fwrite(buffer, sizeof(char), taille, file);
//        if(written < taille) {
//            if(ferror(file) != 0){
//            perror("Serveur : erreur lors de l'écriture du fichier\n");
//            fclose(file);
//            pthread_exit(NULL);
//            } else {
//                printf("Serveur : arrivé a la fin du l'écriture du fichier\n");// fin du fichier
//                break;
//            }
//        }
//    }
//    printf("Serveur : ecriture dans fichier reussie.\n");
//    printf("Serveur : path : %s (taille : %i octets)\n", filepath, file_size);
//    // fermeture du fichier
//    free(filepath);
//    fclose(file);
//    close(dsClient);
//    pthread_exit(NULL);
//}


int main(int argc, char *argv[]) {

    if (argc != 2){
        printf("utilisation : %s port_serveur\n", argv[0]);
        exit(1);
    }

    /* Etape 1 : créer une socket */   
    int ds = creerSocket();
    
    /* Etape 2 : Nommer la socket du serveur */
    struct sockaddr_in ad = nommerSocket(argv[1],ds);
    // afficher port
    socklen_t sizeAd = sizeof(ad);
    getsockname(ds, (struct sockaddr*) &ad, &sizeAd);
    printf("port: %i\n",ntohs(ad.sin_port));

    /* Etape 4: se connecter au client */
    struct sockaddr_in sockClient;
    socklen_t lgAdr;
    char str[INET_ADDRSTRLEN];
    int nbMaxAttente = 10;
    
    //écouter
    int resListen = listen(ds, nbMaxAttente);
    if (resListen == -1) {
        printf("Problème lors de l'écoute\n");
        printf("errno: %d , %s\n", errno, strerror(errno));
        exit(1);    
    }

    int dsClient;
    pthread_t thread[100];
    struct paramCreationServeur params[100];
    int pos = 0;
    /* Etape 4.5 : recevoir un message du client (voir sujet pour plus de détails)*/
    while (1) {
        //accepter une connexion
        dsClient = accept(ds, (struct sockaddr*)&sockClient, &lgAdr);

        if (dsClient == -1) {
            printf("Problème lors de la connexion\n");
            printf("errno: %d , %s\n", errno, strerror(errno));
            exit(1);
        } else {
            inet_ntop(AF_INET, &sockClient.sin_addr, str, INET_ADDRSTRLEN);
            printf("Connexion à l'adresse %s\n",str);
            
            params[pos].dsClient = dsClient;
            params[pos].ds = ds;

            printf("Création du client n°%i\n",pos+1);
            if (pthread_create(&thread[pos], NULL, fonctionServeur, &params[pos]) != 0){
                perror("erreur creation thread");
                exit(1);
            }
            pos++;
        }        
    }
    //fermer le lien à la socket cliente
    printf("Connexion à l'adresse %s fermée. \n", str);
    /* Etape 6 : fermer la socket (lorsqu'elle n'est plus utilisée)*/
    close(ds);
    printf("Serveur : je termine\n");
    return 0;

}
