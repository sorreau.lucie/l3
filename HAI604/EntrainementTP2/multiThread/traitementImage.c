#include <sys/types.h>
#include <pthread.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "calcul.h"


// structure qui regroupe les variables partag�es entre les threads.
struct varPartagees {
  int nbZones;
  int * di; // le tableau indiqu� dans l'�nonc�

  pthread_cond_t cond;
  pthread_cond_t lock;


};
// structure qui regroupe les param�tres d'un thread
struct params {
  int idThread; // cet entier correspond au num�ro de traitement associ� � un thread
  struct varPartagees * vPartage;
};

// fonction associe � chaque thread secondaire � cr�er.

void * traitement (void * p) {

  struct params * args = (struct params *) p;
  struct  varPartagees *  vPartage = args -> vPartage;

  printf("Debut du traitement : %i\n", args->idThread);

  for(int i = 1; i <= vPartage -> nbZones; i++){
    pthread_mutex_lock(&vPartage->lock); // On lock les variable partagées

    if( args -> idThread != 0){ // le premier traitement n'attent personne
      while(vPartage->di[i] != args->idThread -1){
        printf("Traitement %i attente de la zone %i\n", args->idThread,i);
   	// attente fin traitement sur la zone i
      pthread_cond_wait(&vPartage->cond, &vPartage->lock); // endors le thread
      }
    }

    pthread_mutex_unlock(&vPartage->lock);

      // dans cette partie, le traitement de la zone i est � faire en faisant une simulation d'un long calcul (appel a calcul(...)
    printf("Traitement n°%i de la zone %i \n", args->idThread, i);
    calcul(2 + args->idThread);

    printf("Fin du traitement n°%i de la zone %i \n", args->idThread, i);
    pthread_mutex_lock(&vPartage->lock); // bloque la donnée du thread
    vPartage->di[i] = args->idThread;

      // a la fin du traitement d'une zone, le signaler pour qu'un thread en attente se r�veille.
      pthread_cond_broadcast(&vPartage->cond); // reveil le thread 
      pthread_mutex_unlock(&vPartage->lock); // debloque la données du thread

  }

  pthread_exit(NULL);
}




int main(int argc, char * argv[]){

  if (argc!=3) {
    printf(" argument requis \n");
    printf("./prog nombre_Traitements nombre_Zones \n");
    exit(1);
  }


   // initialisations
  pthread_t threads[atoi(argv[1])];
  struct params tabParams[atoi(argv[1])];
  struct varPartagees vPartage;

  vPartage.nbZones =  atoi(argv[2]);
  vPartage.di = malloc(atoi(argv[2])*sizeof(int));

   int err;
  if ((err = pthread_mutex_init(&vPartage.lock, NULL)) != 0) {
      printf("Erreur : %s\n", strerror(err));
      exit(EXIT_FAILURE);
  }

  if ((err = pthread_cond_init(&(vPartage.cond), NULL)) != 0) {
      printf("Erreur : %s\n", strerror(err));
      exit(EXIT_FAILURE);
  }

  srand(atoi(argv[1]));  // initialisation de rand pour la simulation de longs calculs

  // création des threards
  for (int i = 0; i < atoi(argv[1]); i++){
    tabParams[i].idThread = i;
    tabParams[i].vPartage = &vPartage;
    if (pthread_create(&threads[i], NULL,traitement, &tabParams[i]) != 0){
      perror("erreur creation thread");
      exit(1);
    }
  }


  // attente de la fin des  threards. Partie obligatoire
  for (int i = 0; i < atoi(argv[1]); i++){
    pthread_join(threads[i],NULL);
  }
  printf("thread principal : fin de tous les threads secondaires");

  // lib�rer les ressources avant terminaison
  pthread_cond_destroy(&vPartage.cond);
  pthread_mutex_destroy(&vPartage.lock);
  free(vPartage.di);
  return 0;
}

