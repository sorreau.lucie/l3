#include <stdio.h>
#include <sys/types.h>
#include <pthread.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

#include "calcul.h"

struct paramsThreads {

  // structure pour regrouper les param�tres d'un thread. 

  int idThread; // un identifiant de thread, de 1 � N (N le nombre
		// total de theads secondaires
		
  int n; // nombre de thread
  struct variableShared *varPartagee; // Pointeur vers une structure de variables partagés
  // Pointeur pour qu'il se réfère tous a la même structure

};

// structure qui regroupe les données partag�es entre les threads.
struct variableShared {

// parfois il faut déclarer puis affecter, ou on peut juste initialiser des var
  //... // variable nécessitant d'être partagées 

  pthread_cond_t cond; // Obligatoire pour pouvoir endormir / reveiller  les thread après
  pthread_mutex_t lock; // Obligatoire pour lock / unlock la données de chaque thread endormis
    int sharedData; // numéro du thread 

};


// Fonction qui exécute chaque Threads 

void * fonctionThread(void *p){

  struct paramsThreads *args = (struct paramsThreads *) p; // cast paramsThreads en paramsFonctionThread
  struct variableShared * predicat = args -> varPartagee;

  // Le reste dépend du problème 

      //... // Chose a faire / vérifier avant de lock 

    // Lock de la donnée traité 

    pthread_mutex_lock(&predicat->lock);

    if(predicat->sharedData< ...){ // Vérification qu'on a pas atteint le dernier thread

    pthread_cond_wait(&predicat->cond, &predicat->lock); // mise en sommeil des thread en attendant d'atteindre le dernier

    }
    //Verifier qu'on a atteint le dernier thread 
    // une fois arrivé au dernier thread
    pthread_cond_broadcast(&predicat->cond); // reveil des threads



    // déverouiller la donnée

    pthread_mutex_unlock(&predicat->lock); // unlock de la donnée traité



    // Dernière ligne de la fonction

      pthread_exit(NULL); // Ferme tout les threads 

}


int main(int argc, char *argv[]){

    if(argc != 2 ){
        printf("Utilisation: %s nombre_threads\n", argv[0]);
        exit(1);
    }

      // initialisations 
    pthread_t threads[atoi(argv[1])];
    struct params tabParams[atoi(argv[1])];
    struct variableShared predicat;
    predicat.sharedData = 0; // on initialise le numéro du thread a 0


    int err;
    if ((err = pthread_mutex_init(&predicat.lock, NULL)) != 0) { // bloque la donn�e pour pas qu'elle ne soit modifi� par un autre thread
        printf("Erreur : %s\n", strerror(err));
        exit(EXIT_FAILURE);
    }
    if ((err = pthread_cond_init(&predicat.cond, NULL)) != 0) {
        printf("Erreur : %s\n", strerror(err));
        exit(EXIT_FAILURE);
    }


      // cr�ation des threards 
  for (int i = 0; i < atoi(argv[1]); i++){
    tabParams[i].idThread = i + 1;
    tabParams[i].n=atoi(argv[1]);
    tabParams[i].varPartagee = &predicat;

    if (pthread_create(&threads[i], NULL,fonctionThread,(void*)&tabParams[i]) != 0){
      perror("erreur creation thread");
      exit(1);
    }
  }


  // attente de la fin des  threards. Partie obligatoire 
  for (int i = 0; i < atoi(argv[1]); i++){
      pthread_join(threads[i], NULL); // Le join attent la fin de chacun des threads
    }
    printf("Thread principal: fin de tous les threads secondaires.\n");
  
  
  
  
  // terminer "proprement". !!!!!! Ne pas oublier sinon le PC continue a utiliser les coeurs jusqu'à planter 
  pthread_cond_destroy(&predicat.cond);
  pthread_mutex_destroy(&predicat.lock);
  return 0;


}