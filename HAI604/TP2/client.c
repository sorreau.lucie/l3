#include <stdio.h> 
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdlib.h>
#include<arpa/inet.h>
#include<string.h>

#include "common.c"

/* Programme client */

int main(int argc, char *argv[]) {

socklen_t lgAdr = sizeof(struct sockaddr_in);

  if (argc != 4){
    printf("utilisation : %s ip_serveur port_serveur iterations\n", argv[0]);
    printf("localhost : 127.0.0.1");
    exit(1);
  }
  int iterations = atoi(argv[3]);

  /* Etape 1 : créer une socket */   
  int sockette = socket(PF_INET, SOCK_STREAM, 0);


  if (sockette == -1){
    perror("Client : pb creation socket :");
    exit(1); 
  }

  printf("Client : creation de la socket réussie \n");
  
/* Etape 2 : Nommer la socket du client */

  struct sockaddr_in adServ ;
  adServ.sin_family = AF_INET ;
  inet_pton(AF_INET,argv[1],&(adServ.sin_addr)) ;
  adServ.sin_port = htons((short)atoi(argv[2])) ; 

  /* Etape 3 : Demande de connection */

   int res = connect(sockette,(struct sockaddr *) &adServ, lgAdr);

   if(res == -1){
   perror("Client : pb connect");
   exit(1);
  }

  /*Input du message*/
  char* msg[1500];
  printf("Entrez le message :  \n >> ");
  scanf("%s",msg);
  printf("Client : le client envoie \n | %s  %d fois\n",msg,iterations);

  /* Etape 4 : envoyer un message au serveur  (voir sujet pour plus de détails)*/ 
   char sizemsg[10];
   sprintf(sizemsg,"%d",strlen(msg));

  int octetsTotaux = 0;
  for(int ii = 1; ii <= iterations;ii++)
  {
    if(ii == 1){
      printf("Client : %der envoie\n",ii);
    }else{
        printf("Client : %dième envoie\n",ii);
    }
      

      //ENvoie de la taille du message
      octetsTotaux += sendTCP(sockette,sizemsg,strlen(sizemsg)+1);

      //Attente de confirmation
      recvTCP(sockette, sizemsg, sizeof(sizemsg)) ;

      //ENvoie du message
      res = sendTCP(sockette,msg,strlen(msg)+1);
      octetsTotaux += res;

      printf("Client : message envoyé. Taille : %d octets\n",res);
  }

  printf("Client : %d octets envoyés, %d appels à send()\n",octetsTotaux,iterations*2);

  /* Etape 6 : fermer la socket (lorsqu'elle n'est plus utilisée)*/
  
  
  printf("Client : je termine\n");
  return 0;
}
