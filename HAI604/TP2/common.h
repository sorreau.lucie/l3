#include <stdlib.h>
//#include <unistd.h>

//#include <sys/types.h>

// envoi d'un message en TCP
int sendTCP(int socket, const char *buffer, size_t length);

// reception d'un message en TCP
int recvTCP(int socket, char *buffer, size_t length);