#include <stdio.h> 
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdlib.h>
#include<arpa/inet.h>
#include<string.h>

#include "common.c"

/* Programme serveur */

int main(int argc, char *argv[]) {

  socklen_t lgAdr = sizeof(struct sockaddr_in);

  if (argc != 2){
    printf("utilisation : %s port_serveur\n", argv[0]);
    exit(1);
  }

  /* Etape 1 : créer une socket */   
  int sockette = socket(PF_INET, SOCK_STREAM, 0);

  if (sockette == -1){
    perror("Serveur : pb creation socket :");
    exit(1);
  }

  printf("Serveur : creation de la socket réussie \n");
  
  /* Etape 2 : Nommer la socket du seveur */

  struct sockaddr_in ad ;
  ad.sin_family = AF_INET ;
  ad.sin_addr.s_addr = INADDR_ANY ; 
  ad.sin_port = htons((short)atoi(argv[1])) ; 

  int res = bind(sockette, (struct sockaddr*)&ad, sizeof(ad)) ;

  if(res == -1){
   perror("Serveur : pb bind socket ");
   exit(1);
  }

  printf("Serveur : bind de la socket réussie \n");

  /* Etape 3 Listen de la socket */

  res = listen(sockette,10);

  if(res == -1){
   perror("Serveur : pb listen ");
   exit(1);
  }


  printf("Serveur : attente d'un message ... \n");

  /* Etape 4 : recevoir un message du client (voir sujet pour plus de détails)*/

  struct sockaddr_in sockClient ;
  int dsClient = accept(sockette,(struct sockaddr *)&sockClient,&lgAdr);

  int octectTotaux = 0; int count = 0;
  while(1)
  {
    //REception de la taille du message
    char message [100] ;
    octectTotaux += recvTCP(dsClient, message, sizeof(message)) ;

    //Envoie de confiramation
    sendTCP(dsClient,&message,strlen(message)+1);

    //REceptio du message
    char messagefinale[atoi(message)];
    octectTotaux += recvTCP(dsClient, messagefinale, sizeof(messagefinale)) ;

    count++;
    printf("Serveur : %d octets reçus, %d appels à recv()\n",octectTotaux,count*2);
    printf("Serveur : Message reçu : %s | \n",messagefinale);
  }



  
  /* Etape 6 : fermer la socket (lorsqu'elle n'est plus utilisée)*/
  
  close(sockette);
  
  printf("\nServeur : je termine\n");
  return 0;
}
