
#include <cstdlib>
#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <string.h>
#include <signal.h>
#include <sys/types.h>



#define ERROR -1
#define MAX_MSG_SIZE 1000


typedef int ipc_id; // définition du type ipc_id

typedef struct access_request { // Structure permettant la demande d'accès a la variable partagée
    long mtype; 
    int nproc; // Numéro de processus
} AccessRequest; // Alias de la structure

typedef struct shared_data { // Structure de la variable partagée
    long mtype;
    char message[MAX_MSG_SIZE];
} SharedData; // Alias de la structure


ipc_id id; // déclaration d'une variable de type ipc_id 

// IPC_RMID
//  Effacer immédiatement la file de messages, en réveillant tous les processus écrivains et lecteurs en attente. Ils obtiendront un code d'erreur, et errno aura la valeur EIDRM. Le processus appelant doit avoir les privilèges associés ou bien son UID effectif doit être celui du créateur ou du propriétaire de la file de messages. 
void destructAndExit(){
    if(msgctl(id, IPC_RMID, NULL) == ERROR){ // Ici, on verifie que msgctl ne renvoie pas une erreur, si c'est le cas on supprime l'id et on arrête les processus
        perror("Erreur fatale lors de l'exécution du processus");
    }
    exit(EXIT_FAILURE);
}

void sigHandler(int signum) {
    if(msgctl(id, IPC_RMID, NULL) == ERROR) {
        perror("SIGINT: erreur fatale lors de l'exécution du processus ");
        exit(EXIT_FAILURE);
    }
    
}



int main(int argc, char ** argv){


    // Verification des param 

    // argv[0] : nom prog
    // argv[1] : message
    // argv[2] : chemin fichier
    // argv[3] : clé ipc 
    if(argc !=4){
        printf("Utilisation : %s message \n chemin du fichier pour clé ipc \n clé ipc \n", argv[0]);
        exit(EXIT_FAILURE);
    }

    // Enregistrement du sigHandler pour ctrl+c le programme et détruire l'IPC
    signal(SIGINT, sigHandler);


    char message[MAX_MSG_SIZE]; //Declaration de la var contenant le message passé en param d'une taille max 1000
    strcpy(message, argv[1]); // Récupère le message passé en paramètre


    printf("Message utilisateur côté Pctrl : %s \n", message);


    // Création de la file de message

    // Création de la clé

    key_t key = ftok(argv[2], atoi(argv[3])); // Génération de la clé avec le chemin du fichier et la clé défini 
    
    // Verification que la clé est bien créée

    if(key==ERROR){
        perror("Erreur a la création de la clé");
        exit(EXIT_FAILURE);
    }

    // Création de la file de message

    id = msgget(key, IPC_CREAT|0666); // prend en argument la clé, suivis des droit que l'on veut attacher
                            // IPC_CREAT|0666 crée une nouvel file avec les droit d'accès en lecture ET en écriture
                            // O_RDONLY est une demande d'accès en lecture a une file existante
    // id récupère l'identifiant d'une file existante sinon en crée un avec msgget
    if(id == ERROR){
        perror("Erreur a la création OU a l'accès a la file de message");
        exit(EXIT_FAILURE);
    }

    // Attente de la demande de la ressource partagée
    while(1){
        //Attente d'une demande d'accès
        AccessRequest processusRequest;
        ssize_t results = msgrcv(id,(void *) &processusRequest, sizeof(processusRequest),1, 0);
                                //id mess      pointeur vers struct      taille du message              passe le type du message a 1 -> extrait premier message de type msgtyp

        if(results == ERROR){
            perror("Erreur lors de la demande d'accès a la variable partagée");

            destructAndExit(); // Appel de la fonction plus haut 
        }

        printf("Accès donnée au processus %i\n",processusRequest.nproc);


        // Envoie de la donnée partagée au processus 

        SharedData dataSent = (SharedData){
            .mtype = processusRequest.nproc
        };  // Crée une variable dataSent issue de la structure shared_data 
            // (SharedData) cast  
            // donne au mtype de dataSent le nproc c'est a dire le numero du processus recevant la donnée

        strcpy(dataSent.message, message); // Donne au message de la structure shared_data le message récupéré plus haut
        results = msgsnd(id, (const void *)&dataSent, sizeof(dataSent.message),0); // envoie du message dans la file
                        // id mess      // pointeure vers dataSent,     taille du message
        if(results == ERROR){
            perror("Erreur lors de l'envoie du message dans la file de messages");
            destructAndExit();
        }

        printf("Variable envoyée \n");

        //attente de la récéption de la "finition" de la consultation ou de la modification du messsage
        SharedData dataReceived;
        results = msgrcv(id, (void *) &dataReceived, sizeof(dataReceived),0, 0);
        if(results == ERROR){
            perror("Erreur lors de la réception de la varibable partagée");
        }
        // Ici on copie dans la variable locale de message
        strcpy(message,dataReceived.message);
        printf("Message %s\n", message); // affichage du message      
    
    }


    printf("Fin de l'application.\n");

    return 0;



}


