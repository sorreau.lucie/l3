// Compiler :

// gcc -c rdv.c
// gcc calculC.o rdv.o -o RDV -lpthread




#include <sys/types.h>
#include <pthread.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "calcul.h"

struct predicatRdv {

// regrouoes les donn�e partag�es entres les threads participants aux RdV :
pthread_mutex_t lock; // cr�e la variable lock de type mutex
pthread_cond_t cond; // cr�e la variable cond qui est une condition de type cond 
int sharedData; // numéro du thread 

};

struct params {

  // structure pour regrouper les param�tres d'un thread. 

  int idThread; // un identifiant de thread, de 1 � N (N le nombre
		// total de theads secondaires
		
  int n; // nombre de thread
  struct predicatRdv *varPartagee; // cr�e un pointeur varPartagee de type pr�dicatRdv 


};

// fonction associ�e a chaque thread participant au RdV.

void * participant (void *p){ 

  struct params *args = (struct params *) p;
  struct predicatRdv * predicat = args -> varPartagee;
  
  int wait = args->idThread + rand() % 5; // Donne un temps d'attente
  
  printf("[Thread : %i] : Début du calcul. Attente de  : %i secondes \n", args->idThread, wait *2);
  // simulation d'un long calcul pour le travail avant RdV
  calcul (wait); // Fonction du fichier calcul.H qui simule un calcul 

  // RdV 
  pthread_mutex_lock(&predicat->lock); // Lock vérouille la donnée traité
  predicat->sharedData += 1; // Parcours les threads
  if (predicat->sharedData < args->n){  // Ici on vérifie qu'on a pas parcourus 
   pthread_cond_wait(&predicat->cond, &predicat->lock); // On fais dormir les thread jusqu'au calcul du dernier
  }
  else if(predicat->sharedData == args->n){ // Une fois arrivé au dernier thread on lance un broadcast
    printf("[Thread : %i] : Rendez-vous atteint, reveil des threads. \n", args->idThread);
    pthread_cond_broadcast(&predicat->cond); // reveil les thread mis en attente sur la variable de condition 
  }

  pthread_mutex_unlock(&predicat->lock); // Dévérouille la donnée


  wait = args->idThread + rand() % 5; // On refais un random pour avoir une autre valeur 
  printf("[Thread : %i] : Suite du calcul. Attente : %i secondes. \n", args->idThread, wait*1);
  calcul (wait); // reprise et poursuite de l'execution.

  pthread_exit(NULL); // Ferme tout les threads
}




int main(int argc, char *argv[]){
  
 if (argc != 2) {
        printf("Utilisation: %s nombre_threads\n", argv[0]);
        exit(1);
    }

 
  // initialisations 
  pthread_t threads[atoi(argv[1])];
  struct params tabParams[atoi(argv[1])];
  struct predicatRdv predicat;
  predicat.sharedData = 0; // on initialise le numéro du thread a 0

    int err;
    if ((err = pthread_mutex_init(&predicat.lock, NULL)) != 0) { // bloque la donn�e pour pas qu'elle ne soit modifi� par un autre thread
        printf("Erreur : %s\n", strerror(err));
        exit(EXIT_FAILURE);
    }
    if ((err = pthread_cond_init(&predicat.cond, NULL)) != 0) {
        printf("Erreur : %s\n", strerror(err));
        exit(EXIT_FAILURE);
    }

  srand(atoi(argv[1]));  // initialisation de rand pour la simulation de longs calculs
 
  // cr�ation des threards 
  for (int i = 0; i < atoi(argv[1]); i++){
    tabParams[i].idThread = i + 1;
    tabParams[i].n=atoi(argv[1]);
    tabParams[i].varPartagee = &predicat;

    if (pthread_create(&threads[i], NULL,participant,(void*)&tabParams[i]) != 0){
      perror("erreur creation thread");
      exit(1);
    }
  }

  // attente de la fin des  threards. Partie obligatoire 
  for (int i = 0; i < atoi(argv[1]); i++){
      pthread_join(threads[i], NULL); // Le join attent la fin de chacun des threads
    }
    printf("Thread principal: fin de tous les threads secondaires.\n");
  // terminer "proprement". 
  pthread_cond_destroy(&predicat.cond);
  pthread_mutex_destroy(&predicat.lock);
  return 0;
 
}
 
