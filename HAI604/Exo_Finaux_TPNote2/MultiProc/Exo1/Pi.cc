
#include <cstdlib>
#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <string.h>
#include <signal.h>
#include <sys/types.h>
#include <unistd.h>



#define ERROR -1
#define MAX_MSG_SIZE 1000


typedef int ipc_id;

typedef struct access_request {
    long mtype;
    int nproc;
} AccessRequest;

typedef struct shared_data {
    long mtype;
    char message[MAX_MSG_SIZE];
} SharedData;



int main(int argc, char **argv){


    // argv[0] : nom prog
    // argv[1] : chemin fichier
    // argv[2] : clé ipc 
    if(argc !=3){
        printf("Utilisation : %s \n chemin du fichier pour clé ipc \n clé ipc", argv[0]);
        exit(EXIT_FAILURE);
    }


    // Récupération du numéro de processus
    pid_t nproc = getpid();  // getpid -> renvoie l'id du processus appelant 


    if(nproc < 0){
        printf("Erreur : le processus ne produit un identifiant unique. \n");
        exit(EXIT_FAILURE);
    }


    // Récupération de la file de message


    // Récupération de la clé
    key_t key= ftok(argv[1],atoi(argv[2]));
    if(key == ERROR){
        perror("Erreur lors de la création de la clé");
        exit(EXIT_FAILURE);
    }

    // Création de la file de message

    ipc_id id = msgget(key, IPC_CREAT|0666);// prend en argument la clé, suivis des droit que l'on veut attacher
                            // IPC_CREAT|0666 crée une nouvel file avec les droit d'accès en lecture ET en écriture
                            // O_RDONLY est une demande d'accès en lecture a une file existante
    // id récupère l'identifiant d'une file existante sinon en crée un avec msgget
    if(id == ERROR){
        perror("Erreur : la création de la file a échoué");
        exit(EXIT_FAILURE);
    }

    printf("ID de la file de message : %i\n", id);
    //tant que l'utilisateur veut envoyer des messages, il peut les saisirs
    while(1){
        
        printf("Veuillez saisir un message (q pour quitter) :");

        char message[MAX_MSG_SIZE]; // Variable dans laquel on va stocker le message saisi d'une taille 1000 voir plus haut
        fgets(message, MAX_MSG_SIZE, stdin); // récupération du message saisi et stockage dans la var message

        if(strlen(message) == 2 && message[0] == 'q') // vérification que l'utilisateur saisi un q et tape entrer pour quitter
            break;


        //demande d'accès à la variable de messages pour envoyer un message

        const AccessRequest Askrequest = (AccessRequest){.mtype = 1, .nproc = nproc};
        // Crée une variable Askrequest de type AccessRequest (struct) 
        ssize_t results = msgsnd(id, (const void *) &Askrequest, sizeof(Askrequest), 0); // Envoie du message 
                                                // void * donne tout les types 
        if(results == ERROR){
            perror("Erreur : Problème lors de la demande d'accès de la variable partagée");
            exit(EXIT_FAILURE);
        }


        //Réception de la variable partagée
        SharedData varShared;
        results = msgrcv(id, (void *) &varShared, sizeof(varShared),nproc, 0);
                        // id d u message, (void *)&vardéclaré, taille du message de vardéclaré, num proc qui envoie
        
        if(results == ERROR){
            perror("Erreur : Problème de recéption de la variable partagée");
            exit(EXIT_FAILURE);
        }

        printf("Variable partagée %s\n", varShared.message); 

        strcpy(varShared.message, message); // copy du message dans varShared (struct)
        varShared.mtype = 2; // Passage du type de la variable partagé a 0
        //Envoie du message
        results = msgsnd(id, (const void *)&varShared, sizeof(varShared), 0); // !!!! 
        if(results == ERROR){
            perror("Erreur : Problème de modification de la variable partagée");
            exit(EXIT_FAILURE);
        }


        printf("Variable partagée : %s\n", message);

    }
    printf("Fin de l'application. \n");
    return 0;

}