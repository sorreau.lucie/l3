#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <stdlib.h>
/*

Compilation :

g++ -Wall -Iinclude -c semInit.c -o obj/semInit.o
g++ -o bin/semInit obj/semInit.o


 exemple de cr�taion d'un tableau de s�maphores, dont le nombre
 d'�l�ments et la valeur initiale est pass�e en param�tre du
 programme (dans cet exemple, les �lements sont initialis�s � la m�me valeur)

 */

int main(int argc, char * argv[]){
  
  if (argc!=5) {
    printf("Nbre d'args invalide, utilisation :\n");
    printf("%s nombre-semaphores valeur-initiale fichier-pour-cle-ipc entier-pour-cl�-ipc\n", argv[0]);
    exit(0);
  }
	  
  int clesem = ftok(argv[3], atoi(argv[4])); // Création de la clé 

  int nbSem = atoi(argv[1]); // Récupération du nombre de sémaphore passé en param

  int idSem=semget(clesem, nbSem, IPC_CREAT | IPC_EXCL | 0600); //  retourne l'identifiant de l'ensemble de sémaphores associé à la valeur de clé key
  //Si semflg contient à la fois IPC_CREAT et IPC_EXCL et si un jeu de sémaphore associé à la clé key existe déjà, alors semget() échouera en renseignant errno avec EEXIST. 
  
  /*
    IPC_CREAT : permet de créer une nouvelle entrée s'il n'en existe pas déjà une pour cette clef.
    IPC_EXCL : lorsque ce drapeau est combiné avec le précédent, une erreur est générée si l'entrée existe déjà.
*/
  if(idSem == -1){
    perror("erreur semget : "); // Vérification sur le semget
    exit(-1);
  }

  printf("sem id : %d \n", idSem);


  
  // initialisation des s�maphores a la valeur pass�e en parametre (faire autrement pour des valeurs diff�rentes ):
 
  ushort tabinit[nbSem];
  for (int i = 0; i < nbSem; i++) tabinit[i] = atoi(argv[2]);;
 

  union semun{
    int val;
    struct semid_ds * buf;
    ushort * array;
  } valinit;
  
  valinit.array = tabinit;
// Controle des sémaphores
  if (semctl(idSem, nbSem, SETALL, valinit) == -1){
    perror("erreur initialisation sem : ");
    exit(1);
  }

  /* test affichage valeurs des s�maphores du tableau */
  valinit.array = (ushort*)malloc(nbSem * sizeof(ushort)); // pour montrer qu'on r�cup�re bien un nouveau tableau dans la suite

  if (semctl(idSem, nbSem, GETALL, valinit) == -1){
    // http://manpagesfr.free.fr/man/man2/semctl.2.html -> pour remplacer GETALL en fonction de ce qu'on souhaite tester
    perror("erreur initialisation sem : ");
    exit(1);
  } 


// affichage des sémaphore après initialisation  
  printf("Valeurs des semaphores apres initialisation [ "); 
  for(int i=0; i < nbSem-1; i++){
    printf("%d, ", valinit.array[i]);
  }
  printf("%d ] \n", valinit.array[nbSem-1]);

  free(valinit.array);
  return 0;
}
