/*
* Compilation :
g++ -Wall -Iinclude -c semClient.c -o obj/semClient.o
g++ -o bin/semClient obj/semClient.o


Opération : 

P Vérifie s'il peut lancer le processus 
V Incrémente

 */

#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/wait.h>
#include <stdlib.h>

union semun {
    int val ;                   /* cmd = SETVAL */
    struct semid_ds *buf;       /* cmd = IPC STAT ou IPC SET */
    unsigned short *array;      /* cmd = GETALL ou SETALL */
    struct seminfo* __buf;      /* cmd = IPC INFO (sous Linux) */
};

int main(int argc, char * argv[]){
    
    if (argc!=4) {
        printf("Nbre d'args invalide, utilisation :\n");
        printf("%s nombre-clients fichier-pour-cle-ipc entier-pour-clé-ipc\n", argv[0]);
        exit(0);
    }

    int clesem = ftok(argv[2], atoi(argv[3])); // Création de la clé 
    int nbSem = 2; 
    int nbClient = atoi(argv[1]); // Récupération du nombre de client
    int idSem=semget(clesem, nbSem, 0); //
    if(idSem == -1) {
        perror("erreur semget : ");
        exit(-1);
    }
    printf("sem id : %d \n", idSem);

    int proc = 0;

    for (int i=1; i<nbClient; i++) {
        if (fork() == 0) {proc=i;break;}
    }

    printf("[PROC %i] : je me connecte au serveur\n", proc);
    // Opération P 
    struct sembuf opp; 
    opp.sem_num = 0; // Numéro du sémaphore
    opp.sem_op = -1; // Opération sur le sémaphore -> arg > 0 ajout de la val a semval; Si sem_op est inférieur à zéro, le processus appelant doit avoir l'autorisation de modification sur le jeu de sémaphores. 
    // Valeur de sem_op defini l'opération :
    // si n < 0 l'op est P  avec comme valeur |n| : tentative de decrémenter le sémaphore numéro sem_num de |n|
    // si n > 0 l'op est V : incrémentation de n avec réveil des processus en attente;
    // si n = 0 l"opération est Z : attente que la valeur du sémaphore soit 0
    
    opp.sem_flg = 0; // Option pour l'opération
    int res = semop(idSem, &opp, 1); // Attend que la val du sémaphore 0 devienne nulle puis incrémente de 1 
    //               semid,     Opération a effectuer sur une seule sémaphore, 
    

    // semop(idSem,op,1) pour P,
    // semop(idSem,op+1,1) pour V.
    if (res < 0) {
        printf("[PROC %i] : ", proc);
        perror("problème lors de la décrémentation du sem 0");
        exit(0);
    }

    printf("[PROC %i] : j'ai décrémenté\n", proc);

    opp.sem_num = 1;

    // Opération V -> incrémentation
    res = semop(idSem, &opp, 1);
    if (res < 0) {
        printf("[PROC %i] : ", proc);
        perror("problème lors de l'incrémentation du sem 1");
        exit(0);
    }

    printf("[PROC %i] : j'ai incrémenté\n", proc);

    while(wait(0)!=-1);
    return 0;
}
