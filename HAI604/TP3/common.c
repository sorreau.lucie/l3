#include <stdio.h>
#include <sys/types.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

int sendTCP(int socket, const char *buffer, size_t length){

	ssize_t sent;
	ssize_t total_sent = 0;


	while(length > 0){
		if(sent<= 0){
			return sent;
		}

		buffer +=sent;
		total_sent += sent;
		length -= sent;
	}
	return total_sent;
}

int recvTCP(int socket, const char *buffer, size_t length)
{
	ssize_t received;
	ssize_t total_received = 0;

	while (length > 0)
	{
		received = recv(socket, buffer, length, 0);

		if (received <= 0)
		{
			return received;
		}

		buffer += received;
		total_received += received;
		length -= received;
	}

	return total_received;
}