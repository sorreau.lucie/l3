#include <stdio.h> 
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdlib.h>
#include<arpa/inet.h>
#include<string.h>
#include <sys/stat.h>


#include"sendTCP.h"

#define MAX_BUFFER_SIZE 146980

int main(int argc, char *argv[]) {



  if (argc != 4){
    printf("utilisation : ip_serveur port_serveur fichier\n");
    exit(1);
  }

  socklen_t lgAdr = sizeof(struct sockaddr_in);

  /* Etape 1 : créer une socket */   
  int sockette = socket(PF_INET, SOCK_STREAM, 0);


  if (sockette == -1){
    perror("Client : pb creation socket :");
    exit(1); 
  }

  printf("Client : creation de la socket réussie \n");
  
/* Etape 2 : Nommer la socket du client */

  struct sockaddr_in adServ ;
  adServ.sin_family = AF_INET ;
  inet_pton(AF_INET,argv[1],&(adServ.sin_addr)) ;
  adServ.sin_port = htons((short)atoi(argv[2])) ; 

  /* Etape 3 : Demande de connection */

   int res = connect(sockette,(struct sockaddr *) &adServ, lgAdr);

   if(res == -1){
   perror("Client : pb connect");
   exit(1);
  }

 
  /* etape 4 : envoi de fichier : attention la question est générale. Il faut creuser pour définir un protocole d'échange entre le client et le serveur pour envoyer correctement un fichier */

  int totalSent = 0; // variable pour compter le nombre total d'octet effectivement envoyés au serveur du début à la fin des échanges.
  char filename [50] = "court.txt";
  res = send(sockette, filename, strlen(filename)+1,0);

  if(res <0){
    perror("Serveur : erreur filename");
    exit(1);
  }

  printf("Serveur : je vais recevoir : %s\n", filename);

 
  /* le bout de code suivant est une lecture de contenu d'un fichier dont le nom est passé en paramètre.
     - pour lire un fichier, il faut l'ouvrir en mode lecture
     - la lecture se fait par blocs d'octets jusqu'à la fin du fichier.
     - la taille d'un bloc est définie par la constante MAX_BUFFER_SIZE que vous pouvez modifier.

     Le code est à compléter pour mettre en place l'envoi d'un fichier.
  */

  // construction du nom du chemin vers le fichier
  char* filepath = malloc(strlen(argv[3]) + 16); // ./emission/+nom fichier +\0
  filepath[0] = '\0';
  strcat(filepath, "./emission/");
  strcat(filepath, argv[3]);

  printf("Client: je vais envoyer %s\n", filepath);

  // obtenir la taille du fichier
  struct stat attributes;
  if(stat(filepath, &attributes) == -1){
    perror("Client : erreur stat");
    free(filepath);
    exit(1);
  }

  int file_size = attributes.st_size; // cette copie est uniquement informer d'où obtenir la taille du fichier.
  
  printf("Client : taille du fichier : %d octets\n", file_size);
  
  // lecture du contenu d'un fichier
  FILE* file = fopen(filepath, "rb");
  if(file == NULL){
    perror("Client : erreur ouverture fichier \n");
    free(filepath);
    exit(1);   
  }
  free(filepath);

  int total_lu = 0;
  char buffer[MAX_BUFFER_SIZE];
  while(total_lu < file_size){
    
    size_t read = fread(buffer, sizeof(char), MAX_BUFFER_SIZE, file);
    if(read == 0){
      if(ferror(file) != 0){
	perror("Client : erreur lors de la lecture du fichier \n");
	fclose(file);
	exit(1);
      }else{
        printf("Client : arrivé a la fin du la lecture du fichier\n");// fin du fichier
	break;
      }
    }
    printf("Client : j'ai lu un bloc du fichier \n");  
    total_lu += read;
  }

  // fermeture du fichier
  fclose(file); 
   
  printf("Client : j'ai lu au total : %d octets \n", total_lu);  
 
 
  printf("Client : c'est fini\n");

  close(sockette);
  return 0;
}
