#include <stdio.h>//perror
#include <sys/types.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <unistd.h>//close
#include <stdlib.h>
#include <string.h>


#include"sendTCP.h"

#define MAX_BUFFER_SIZE 146980


int main(int argc, char *argv[])
{

  socklen_t lgAdr = sizeof(struct sockaddr_in); 
  /* etape 0 : gestion des paramètres si vous souhaitez en passer */

  if(argc != 2){
    printf("utilisation : %s port_serveur\n", argv[0]);
    exit(1);
  }
  
  /* etape 1 : creer une socket d'écoute des demandes de connexions*/

  int sockette = socket(PF_INET, SOCK_STREAM, 0);

  if (sockette == -1){
    perror("Serveur : pb creation socket :");
    exit(1);
  }

  printf("Serveur : creation de la socket réussie \n");
  
  /* etape 2 : nommage de la socket */

  struct sockaddr_in ad ;
  ad.sin_family = AF_INET ;
  ad.sin_addr.s_addr = INADDR_ANY ; 
  ad.sin_port = htons((short)atoi(argv[1])) ; 

  int res = bind(sockette, (struct sockaddr*)&ad, sizeof(ad)) ;

  if(res == -1){
   perror("Serveur : pb bind socket ");
   exit(1);
  }

  printf("Serveur : bind de la socket réussie \n");

  /* etape 3 : mise en ecoute des demandes de connexions */
  res = listen(sockette,10);

  if(res == -1){
   perror("Serveur : pb listen ");
   exit(1);
  }


  printf("Serveur : attente d'une demande de connexion ... \n");

 
  /* etape 4 : plus qu'a attendre la demadne d'un client */
 

  struct sockaddr_in sockClient ;
  int dsClient = accept(sockette,(struct sockaddr *)&sockClient,&lgAdr);

  printf("Serveur : Demande accepté\n");
 
  int totalRecv = 0; // un compteur du nombre total d'octets recus d'un client

 // char* buffer[MAX_BUFFER_SIZE];
  
  /* le protocol d'echange avec un client pour recevoir un fichier est à définir. Ici seul un exemple de code pour l'écriture dans un fichier est founi*/
   
  char filename [50];
  res = recv(dsClient, filename, sizeof(filename),0);
  if(res <0){
    perror("Serveur : erreur filename");
    exit(1);
  }

      printf("Serveur : je vais recevoir : %s\n", filename);


  char* filepath = malloc(strlen(filename)+17); // cette ligne n'est bien-sur qu'un exemple et doit être modifiée : le nom du fichier doit être reçu.
  filepath[0] = '\0';
  strcat(filepath,"./reception/");
  strcat(filepath, filename);
  printf("Serveur : je vais recevoir : %s\n", filepath);



  printf("Serveur : fichier a recevoir : "); 
  // On ouvre le fichier dans lequel on va écrire
  FILE* file = fopen(filepath, "wb");
  if(file == NULL){
    perror("Serveur : erreur ouverture fichier: \n");
    exit(1);  
  }

  char* buffer ="ceci est un exemple de contenu a ecrire dans un fichier\n";
  size_t written = fwrite(buffer, sizeof(char), strlen(buffer)+1, file);
  if(written < strlen(buffer)+1){  // cette ligne est valide uniquement pour ce simple exemple
    perror("Serveur : Erreur a l'ecriture du fichier \n");
    fclose(file); 
  }

  printf("Serveur : ecriture dans fichier reussie. Vous pouvez vérifier la création du fichier et son contenu.\n");
  // fermeture du fichier
  fclose(file);
    
  printf("Serveur : c'est fini\n");


  close(sockette);
}








