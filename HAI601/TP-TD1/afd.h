/**
 * @file afd.h     
 * @author Michel Meynard
 * @brief Définition d'un AFD
 */
#define EINIT 0
#define EI 1
#define EIF 2
#define EID 3
#define ESEP 4
#define ECHIFFRE 5
#define EFLOAT 6
#define EP 7
#define ESLASH 8
#define ESLASHET 9
#define ESLASHDET 10
#define ECOM2 11
#define EDSLASH 12
#define ECOM 13

int TRANS[NBETAT][256];		/* table de transition : état suivant */
int JETON[NBETAT];		/* jeton à retourner */

int creerAfd(){			/* Construction de l'AFD */
  int i;int j;			/* variables locales */
  for (i=0;i<NBETAT;i++){
    for(j=0;j<256;j++) TRANS[i][j]=-1; /* init vide */
    JETON[i]=0;			/* init tous états non finaux */
  }
  /* Transitions de l'AFD */
  TRANS[EINIT]['a']=EA;TRANS[EA]['b']=EAB;TRANS[EAB]['b']=EAB;
  TRANS[EAB]['c']=EABC;TRANS[EINIT]['b']=EB;TRANS[EB]['d']=EBD; 
  JETON[EA]=JETON[EABC]=1;
  JETON[EBD]=-1; /* états finaux */
}



/** construit un ensemble de transitions de ed à ef pour un intervale de char
*@param ed l'état de départ
*@param cd char de début
*@param cf char de fin
* *@param ef l'état final
*/

void classe(int ed, int cd, int cf, int ef){
  for(int i = cd; i<cf; i++){
    TRANS[ed][i] = ef;
  }
}


