%{
/* ZONE DE DEFINITION (OPTIONNELLE) */
enum Regles {
    MOT = 1,
    RETOUR,
    BALEC
};

%}
/* ZONE DES REGLES apres le double pourcent (OBLIGATOIRE) */
%%
\n              {return RETOUR; }
[^\ \n\t]+      {return MOT;    }
.               {return BALEC;  }
%%

/* ZONE DES FONCTIONS C */
int main(int argc, char** argv) {
    if (argc < 2) {
        printf("%s fichier.in", argv[0]);
        exit(1);
    }

    yyin = fopen(argv[1],"r");

    int caracteres = 0;
    int lignes = 0;
    int mots = 0;

    int j; 
    while ((j=yylex())!=0) {
        if (j == MOT) mots++;
        if (j == RETOUR) lignes++;
        caracteres += yyleng;        
    }
    fclose(yyin);

    printf("%i %i %i\n",lignes,mots,caracteres);

    return 0;
}