%{
/* ZONE DE DEFINITION (OPTIONNELLE) */
enum Regles {
    AUTRE = 1,
    LIGNEBLANCHE,
    DEBUTBLANC,
    FINBLANCHE,
    MULTIPLEBLANCS,
    TABULATION
};

%}
/* ZONE DES REGLES apres le double pourcent (OBLIGATOIRE) */
%%
^(\ |\t|\n)*\n      {return LIGNEBLANCHE;   } 
^(\ |\t)+           {return DEBUTBLANC;     }                     
(\ |\t)+\$          {return FINBLANCHE;     }
(\t\ )+             {return MULTIPLEBLANCS; }
\t+                 {return TABULATION;     }
.|\n                {return yytext[0];      }
%%

/* ZONE DES FONCTIONS C */
int main(int argc, char** argv) {
    if (argc < 3) {
        printf("%s fichier.in fichier.out", argv[0]);
        exit(1);
    }
    yyin = fopen(argv[1],"r");
    FILE* wFile = fopen(argv[2],"w");

    int j; 
    while ((j=yylex())!=0) {
        if(j != LIGNEBLANCHE && j != FINBLANCHE && j != DEBUTBLANC) {
            if (j == TABULATION || j == MULTIPLEBLANCS) {
                fprintf(wFile, "%s", " ");
            } else {
                fprintf(wFile, "%s", yytext);
            }
        }
        
    }
    fclose(yyin);
    fclose(wFile);

    return 0;
}