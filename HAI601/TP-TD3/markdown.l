%{
/* ZONE DE DEFINITION (OPTIONNELLE) */
enum Regles {
    AUTRE = 1,
    RETOUR,
    TABULATION,
    OUVERTURE,
    LIGNEBLANCHE,
};

%}
/* ZONE DES REGLES apres le double pourcent (OBLIGATOIRE) */
%%
^\n                  {return LIGNEBLANCHE;}
\n                   {return RETOUR;}
(\t|\ \ \ \ )        {return TABULATION;}
[\*\-]               {return OUVERTURE;}
.                    {return AUTRE;}
%%

/* ZONE DES FONCTIONS C */
int main(int argc, char** argv) {
    if (argc < 3) {
        printf("%s fichier.in fichier.out", argv[0]);
        exit(1);
    }
    yyin = fopen(argv[1],"r");
    FILE* wFile = fopen(argv[2],"w");

    int indentCur = 0;      // recuperer le compte d'indentation
    int indentPrec = 0;     // conserver le précédent compte d'indentation
    char prec = 'a';        // conserver le precedent char d'ouverture
    int ouverts = 0;        // compter le nombre de listes ouvertes
    int dansListe = 0;      // est on sur cette ligne dans une liste déjà ouverte?

    int j; 
    while ((j=yylex())!=0) {

        if(j == AUTRE || j == RETOUR) {         // autre ou retour: on l'écrit dans le fichier
            fprintf(wFile, "%s", yytext);
            if (j == RETOUR) {                  // retour:
                if(ouverts > 0) {
                    fprintf(wFile, "%s", "</li>");      // fermer une puce
                }
                indentPrec = indentCur;         // conserver l'actuelle indentation 
                indentCur = 0;
                printf("RETOUR; prec: %i, cur: %i\n",indentPrec, indentCur);
                dansListe = 0;                  // nouvelle ligne, pas encore dans une liste
            }

        } else if(j == TABULATION) {            // tabulation: les compter
            if(dansListe == 0) {
                indentCur++;
            } else {
                fprintf(wFile, "%s", yytext);   // si on est dans une liste, on l'écrit plutôt
            }

        } else if(j == OUVERTURE) {             // ouverture de liste
            if (dansListe) {                    // on est déjà dans une liste, on l'écrit
                fprintf(wFile, "%s", yytext);  
            } else {
                dansListe = 1;                  // on signale qu'on rentre dans une liste
                printf("prec: %i; cur: %i\n",indentPrec,indentCur);
                if (indentPrec < indentCur) {   // on a augmenté l'indentation: nouvelle liste
                    int diff = indentCur - indentPrec;
                    while (diff > 0) {                  // fermer plusieurs listes ?
                        fprintf(wFile, "%s", "<ul>"); 
                        ouverts++;
                        diff--;
                    }
                } else if (indentCur < indentPrec) {    // on a diminué l'indentation
                    int diff = indentPrec-indentCur;
                    while (diff > 0) {                  // fermer plusieurs listes ?
                        fprintf(wFile, "%s", "</ul>"); 
                        ouverts--;
                        diff--;
                    }
                } else if (yytext[0] != prec) {      // si c'est un autre caractère d'ouverture
                    ouverts++;
                    fprintf(wFile, "%s", "<ul>");
                }
                prec = yytext[0];                    // conserver le caractère d'ouverture
                fprintf(wFile, "%s", "<li>");
            }

        } else if (j == LIGNEBLANCHE) {              // fin ou début de liste: on réinitialise
            printf("ouverts: %i\n",ouverts);
            while (ouverts > 0) {
                fprintf(wFile, "%s", "</ul>");
                ouverts--;
            }
            indentCur = 0;
            indentPrec = 0;
            prec = 'a';
            dansListe = 0;
        }
    }

    fclose(yyin);
    fclose(wFile);

    return 0;
}