%{ /* analflex.l */
%}
chiffre ([0-9])
lettre ([a-zA-Z])
%%
if {return 300;}
else {return 301;}
then {return 302;}
throw {return 303;}
0[0-7]+ {return 304;} // octal
0x[0-9A-Fa-f]+ {return 305;} //hexa
[1-9]{chiffre}* {return 306;} // decimal
{lettre}({lettre}|{chiffre}|_)* {return 307;} //id 
({chiffre}+\.{chiffre}*|\.{chiffre}+)([eE][-+]?{chiffre}+)? {return 308;} //flottant
[ \t\n]+ {} 
"/ /".*\n {}
"/*"([^*]|"*"+[^*/])*"*"+"/" {}
. {return yytext[0];}
%%
int main(){
int j; 
char *invite="Saisissez un(des) mot(s) {Lkey Lc10 Lc8 Lid Lf Lsep} suivi de EOF(CTRL-D) SVP : ";
printf(invite);
while ((j=yylex())!=0)
printf("\nJeton : %i; de lexeme : %s\n%s",j,yytext,invite);
return 1;
}
