// histo.cpp : tableau des niveaux de gris

#include <stdio.h>
#include "image_ppm.h"
#include <iostream>
#include <fstream>

int main(int argc, char* argv[])
{
    char cNomImgLue[250],cNomImgEcrite[250];
    int nH, nW,nTaille;
    float ddp[256], histo[256], F[256];
    for (int i=0;i<256;i++)histo[i]=0;
    if (argc != 3) 
     {
       printf("Usage: ImageIn.pgm ImageOut.pgm \n"); 
       exit (1) ;
     }

     sscanf (argv[1],"%s",cNomImgLue) ;
     sscanf (argv[2],"%s",cNomImgEcrite);

     OCTET *ImgIn, *ImgOut;
     lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
        nTaille = nH * nW;

  
        allocation_tableau(ImgIn, OCTET, nTaille);
        lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
           allocation_tableau(ImgOut, OCTET, nTaille);

        for (int i=0; i < nH; i++){
            for (int j=0; j < nW; j++)
           {
               histo[ImgIn[i*nW+j]]++;
           }
          }

            
    
    for (int i=0;i<256;i++){
        ddp[i] = histo[i] / nTaille;
    }
    
        F[0] = ddp[0];

    for (int i=1;i<256;i++){
        F[i] = F[i-1]+ ddp[i];

    }


    for (int i=0; i < nH; i++){
        for (int j=0; j < nW; j++){

               ImgOut[i*nW+j] = F[ImgIn[i*nW+j]]*255;
        }
    }



    ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
    free(ImgIn);free(ImgOut);
    return 1;
}