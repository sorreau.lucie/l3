// test_couleur.cpp : Seuille une image en niveau de gris

#include <stdio.h>
#include <iostream>
#include <fstream>
#include "image_ppm.h"
using namespace std;

void filtreCouleur(char *cNomImgLue, char *cNomImgEcrite)
{
    int nH, nW, nTaille;
    OCTET *ImgIn, *ImgOut;

    lire_nb_lignes_colonnes_image_ppm(cNomImgLue, &nH, &nW);
    nTaille = nH * nW;
    int taille3 = nTaille * 3;
    allocation_tableau(ImgIn, OCTET, taille3);
    allocation_tableau(ImgOut, OCTET, taille3);
    lire_image_ppm(cNomImgLue, ImgIn, nTaille);

    for (int i = 0; i < taille3; i += 3)
    {
        for (int j = 0; j < 3; j++)
        {
            int h = i + j;

            if ((h - (nW * 3)) < 0 || (h + (nW * 3)) >= taille3)
            {
                ImgOut[h] = ImgIn[h];
            }
            else
            {
                ImgOut[h] = (ImgIn[h] +
                             ImgIn[h - (nW * 3)] +
                             ImgIn[h + (nW * 3)] +
                             ImgIn[h - 3] +
                             ImgIn[h + 3]) /
                            5;
            }
        }
    }

    ecrire_image_ppm(cNomImgEcrite, ImgOut, nH, nW);
    free(ImgIn);
    free(ImgOut);
}

int main(int argc, char *argv[])
{
    char cNomImgLue[250], cNomImgEcrite[250];
    int count;

    if (argc != 4)
    {
        printf("Usage: ImageIn.ppm  ImgOut.ppm count\n");
        exit(1);
    }

    sscanf(argv[1], "%s", cNomImgLue);
    sscanf(argv[2], "%s", cNomImgEcrite);
    sscanf(argv[3], "%d", &count);

    filtreCouleur(cNomImgLue, cNomImgEcrite);
    for (int i = 0; i < count - 1; i++)
    {
        filtreCouleur(cNomImgEcrite, cNomImgEcrite);
    }

    return 1;
}
