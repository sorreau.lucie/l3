// histo.cpp : tableau des niveaux de gris

#include <stdio.h>
#include "image_ppm.h"
#include <iostream>
#include <fstream>

int main(int argc, char* argv[])
{
    char cNomImgLue[250];
    int nH, nW,nTaille;
    float tab[256], histo[256];
    for (int i=0;i<256;i++)histo[i]=0;
    if (argc != 2) 
     {
       printf("Usage: ImageIn.pgm \n"); 
       exit (1) ;
     }

     sscanf (argv[1],"%s",cNomImgLue) ;

     OCTET *ImgIn;
     lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
        nTaille = nH * nW;

  
        allocation_tableau(ImgIn, OCTET, nTaille);
        lire_image_pgm(cNomImgLue, ImgIn, nH * nW);

        for (int i=0; i < nH; i++){
            for (int j=0; j < nW; j++)
           {
               histo[ImgIn[i*nW+j]]++;
           }
          }

            
          
          
    free(ImgIn);
    std::ofstream f("ddp.dat");
    for (int i=0;i<256;i++){
        tab[i] = histo[i] / nTaille;
        f <<i<< "  " <<tab[i]<<"\n";
    }
    
    
    return 1;
}