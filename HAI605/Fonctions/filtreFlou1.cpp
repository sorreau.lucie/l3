// filtreFlou1.cpp 

#include <stdio.h>
#include <iostream>
#include <fstream>
#include "image_ppm.h"
using namespace std;

int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250], colorline;
  int nH, nW, nTaille;
  
  if (argc != 3) 
     {
       printf("Usage: ImageIn.pgm  ImgOut.pgm \n"); 
       exit (1) ;
     }

   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgEcrite);

   OCTET *ImgIn,*ImgOut;
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn, OCTET, nTaille);
   allocation_tableau(ImgOut, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);

  for(int i = 0; i < nW;i++)
  {
    for(int j = 0; j < nH;j++)
    {
      if(i==0||j==0||i==nW-1||j==nH-1)
      {
        ImgOut[j+(i*nH)] = ImgIn[j+(i*nH)];
      }
      else
      {
        ImgOut[j+(i*nH)] = (
          ImgIn[j+(i*nH)] +
          ImgIn[(j-1)+(i*nH)] +
          ImgIn[(j+1)+(i*nH)] +
          ImgIn[j+((i-1)*nH)] +
          ImgIn[j+((i+1)*nH)] ) / 5;
      }
    }
  }

   ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
   free(ImgIn); free(ImgOut);

   return 1;
}
