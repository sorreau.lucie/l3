#include <stdio.h>
#include "image_ppm.h"

int main(int argc, char* argv[])
{
  char cNomImg1[250], cNomImg2[250];
  int nH, nW, nTaille;
  
  if (argc != 3) 
     {
       printf("Usage: ImageOriginal.pgm Image.pgm \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImg1) ;
   sscanf (argv[2],"%s",cNomImg2);

   OCTET *Img1, *Img2;
   
   lire_nb_lignes_colonnes_image_pgm(cNomImg1, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(Img1, OCTET, nTaille);
   lire_image_pgm(cNomImg1, Img1, nH * nW);
   allocation_tableau(Img2, OCTET, nTaille);
   lire_image_pgm(cNomImg2, Img2, nH * nW);

  int m = 0;
 for (int i=0; i < nTaille; i++)
   {
     int v = (Img1[i]-Img2[i]);
      m += v*v;
  }
  m /= nTaille;
  
 printf("L'erreur moyenne quadratique est de : %d\n", m);
       
   free(Img1); free(Img2);

   return 1
