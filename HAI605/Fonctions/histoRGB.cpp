// histo_rgb.cpp : histogramme d'une image en couleur
#include <stdio.h>
#include "image_ppm.h"
#include <iostream>
#include <fstream>
using namespace std;

int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomHistoEcrit[250];
  int nH, nW, nTaille;
  
  if (argc != 3) 
     {
       printf("Usage: ImageIn.pgm HistoOut.dat \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomHistoEcrit);

   OCTET *ImgIn, *ImgOut;
   
   lire_nb_lignes_colonnes_image_ppm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   int nTaille3 = nTaille * 3;
   allocation_tableau(ImgIn, OCTET, nTaille3);
   lire_image_ppm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille3);

	int histo[256*3];
  for(int i = 0; i < 256*3;i++){histo[i]=0;}

   for (int i=0; i < nTaille3; i+=3)
     {
      histo[ImgIn[i]]++;
      histo[256+(ImgIn[i+1])]++;
      histo[512+(ImgIn[i+2])]++;
     }

   ofstream flux(cNomHistoEcrit);
   for(int i =0;i < 256;i++)
   {
     flux << i << " " << histo[i] << " " << histo[256+i] << " " << histo[512+i] << endl;
   }
   free(ImgIn); 
   return 1;
}
