uniform float ambientRef;
uniform float diffuseRef;
uniform float specularRef;
uniform float shininess;


varying vec4 p;
varying vec3 n;


void main (void) {


vec3 P = vec3(gl_ModelViewMatrix * p);
vec3 N = normalize(gl_NormalMatrix * n);
vec3 V = normalize(-P); //Vecteur de vue


// Ambiante


vec4 Isa = gl_LightModel.ambient;
vec4 Ka = gl_FrontMaterial.ambient;
vec4 Ia = Isa * Ka;
vec4 I = ambientRef * Ia;


for (int i = 0; i < 3; ++i) {
// Diffuse
vec4 Isd = gl_LightSource[i].diffuse;
vec4 Kd = gl_FrontMaterial.diffuse;
vec3 L = normalize(gl_LightSource[i].position.xyz - P);
vec4 Id = Isd * Kd * max(dot(N, L), 0.);
I += Id * diffuseRef;


// Spéculaire
vec4 Iss = gl_LightSource[i].specular;
vec4 Ks = gl_FrontMaterial.specular;
vec3 R = (2. * dot(N, L) * N) - L;
vec4 Is = Iss * Ks * pow(max(dot(R, V), 0.), shininess);
I += Is * specularRef;
}
gl_FragColor = vec4 (I.xyz, 1);
}