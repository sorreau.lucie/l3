// test_couleur.cpp : Seuille une image en niveau de gris

#include <stdio.h>
#include "image_ppm.h"

void erosion(int argc, char* cNomImgLue, char* cNomImgEcrite)
{

  int nH, nW, nTaille;
 
   OCTET *ImgIn, *ImgOut;
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille);
  

 for (int i=1; i < nH-1; i++){
   for (int j=1; j < nW-1; j++)
     {
       int px = ImgIn[i*nW + j];
       if (px == 255){
        ImgOut[i*nW+j]=255;
        continue;
       }

       int* voisin = new int[4]{
        ImgIn[(i-1)*nW+j],
        ImgIn[(i+1)*nW+j],
        ImgIn[i*nW+(j-1)],
        ImgIn[i*nW+(j+1)]};


        int cpt = 0;
        for(int k = 0; k < 4; k ++){
          if(voisin[k] == 255){
            cpt = cpt + 1;
          }

           if(cpt > 0){
        ImgOut[i*nW+j] = 255;

       }else{
        ImgOut[i*nW+j] = ImgIn[i*nW+j];
       }

        }

       }

     }

   ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
   free(ImgIn); free(ImgOut);

}

void dilatation(int argc, char* cNomImgLue, char* cNomImgEcrite)
{

  int nH, nW, nTaille;
  

   OCTET *ImgIn, *ImgOut;
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille);
  

 for (int i=1; i < nH-1; i++){
   for (int j=1; j < nW-1; j++)
     {
       int px = ImgIn[i*nW + j];
       if (px == 0){
        ImgOut[i*nW+j]=0;
       }

       int* voisin = new int[4]{
        ImgIn[(i-1)*nW+j],
        ImgIn[(i+1)*nW+j],
        ImgIn[i*nW+(j-1)],
        ImgIn[i*nW+(j+1)]};


        int cpt = 0;
        for(int k = 0; k < 4; k ++){
          if(voisin[k] == 0){
            cpt = cpt + 1;
          }

           if(cpt > 0){
        ImgOut[i*nW+j] = 0;

       }else{
        ImgOut[i*nW+j] = ImgIn[i*nW+j];
       }

        }

       }

     }

   ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
   free(ImgIn); free(ImgOut);

}


int main(int argc, char  *argv[])
{

  char cNomImgLue[250], cNomImgEcrite[250];

    if (argc != 3) 
     {
       printf("Usage: ImageIn.pgm ImageOut.pgm \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgEcrite);


  erosion(argc, cNomImgLue,"file.pgm");
  dilatation(argc,"file.pgm",cNomImgEcrite);
}