// test_couleur.cpp : Seuille une image en niveau de gris

#include <stdio.h>
#include "image_ppm.h"

int main(int argc, char* argv[])
{
  char cNomImgSeuil[250], cNomImgDil[250], cNomImgEcrite[250];
  int nH, nW, nTaille;
  
  if (argc != 4) 
     {
       printf("Usage: ImageInSeuil.pgm ImageInDilate.pgm ImageOut.pgm \n"); 
       exit (1) ;
     }

   sscanf (argv[1],"%s",cNomImgSeuil) ;
   sscanf (argv[2],"%s",cNomImgDil) ;
   sscanf (argv[3],"%s",cNomImgEcrite);

   OCTET *ImgSeuil, *ImgDil, *ImgOut;

   lire_nb_lignes_colonnes_image_pgm(cNomImgSeuil, &nH, &nW);
   nTaille = nH * nW;

   allocation_tableau(ImgSeuil, OCTET, nTaille);
   allocation_tableau(ImgDil, OCTET, nTaille);
   lire_image_pgm(cNomImgSeuil, ImgSeuil, nH * nW);
   lire_image_pgm(cNomImgDil, ImgDil, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille);
	
 for (int i=1; i < nH-1; i++){
   for (int j=1; j < nW-1; j++)
     {
       int seuil = ImgSeuil[i*nW+j];
       int dilatation = ImgDil[i*nW+j];

       if(seuil == 255 && dilatation == 255){
        ImgOut[i*nW+j] = 255;
       }else if(seuil == 0 && dilatation == 0){
        ImgOut[i*nW+j] = 255;
      }else{
        ImgOut[i*nW+j] = 0;
      }

     }
   }

   ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
   free(ImgSeuil); free(ImgDil);free(ImgOut);

   return 1;
}
