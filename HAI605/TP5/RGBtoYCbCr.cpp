
#include <stdio.h>
#include <iostream>
#include <fstream>
#include "image_ppm.h"
using namespace std;

void RGBtoYCrCb(char* cNomImgLue, char* cNomImgY, char* cNomImgCb, char* cNomImgCr)
{
  int nH, nW, nTaille;
  OCTET *ImgIn,*ImgY,*ImgCr,*ImgCb;
   
   lire_nb_lignes_colonnes_image_ppm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
  int t3 = nTaille * 3;
   allocation_tableau(ImgIn, OCTET, t3);
   allocation_tableau(ImgY, OCTET, nTaille);
  allocation_tableau(ImgCr, OCTET, nTaille);
  allocation_tableau(ImgCb, OCTET, nTaille);
   lire_image_ppm(cNomImgLue, ImgIn, nTaille);

  for(int i = 0; i < t3;i+=3)
  {    
      ImgY[i/3] = 
        0.299*ImgIn[i] +
        0.587*ImgIn[i+1] +
        0.114*ImgIn[i+2];

      ImgCr[i/3] = 
        0.5*ImgIn[i] -
        0.4187*ImgIn[i+1] -
        0.0813*ImgIn[i+2] + 128;

      ImgCb[i/3] = 
        -0.1687*ImgIn[i] -
        0.3313*ImgIn[i+1] +
        0.5*ImgIn[i+2] + 128;
    
  }

   ecrire_image_pgm(cNomImgY, ImgY,  nH, nW);
  ecrire_image_pgm(cNomImgCr, ImgCr,  nH, nW);
  ecrire_image_pgm(cNomImgCb, ImgCb,  nH, nW);
   free(ImgIn); free(ImgY); free(ImgCb); free(ImgCb);
}

int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgY[250], cNomImgCr[250], cNomImgCb[250];

  if (argc != 5) 
     {
       printf("Usage: ImageIn.ppm  ImgY.pgm ImgCr.pgm ImgCb.pgm\n"); 
       exit (1) ;
     }

   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgY);
  sscanf (argv[3],"%s",cNomImgCr);
  sscanf (argv[4],"%s",cNomImgCb);
  
  RGBtoYCrCb(cNomImgLue,cNomImgY,cNomImgCb,cNomImgCr);
  
   return 1;
}
