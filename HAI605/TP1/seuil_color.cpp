// seuil_color.cpp : Seuille une image en couleur

#include <stdio.h>
#include "image_ppm.h"

int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille, RedSeuil,GreenSeuil,BlueSeuil;
  
  if (argc != 6) 
     {
       printf("Usage: ImageIn.pgm ImageOut.pgm RedSeuil GreenSeuil BlueSeuil\n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgEcrite);
   sscanf (argv[3],"%d",&RedSeuil);
   sscanf (argv[4],"%d",&GreenSeuil);
  sscanf (argv[5],"%d",&BlueSeuil);

   OCTET *ImgIn, *ImgOut;
   
   lire_nb_lignes_colonnes_image_ppm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   int nTaille3 = nTaille * 3;
   allocation_tableau(ImgIn, OCTET, nTaille3);
   lire_image_ppm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille3);
	
   for (int i=0; i < nTaille3; i+=3)
     {

       if (ImgIn[i] < RedSeuil) ImgOut[i]=0; else ImgOut[i]=255;
       if (ImgIn[i+1] < GreenSeuil) ImgOut[i+1]=0; else ImgOut[i+1]=255; 
       if (ImgIn[i+2] < BlueSeuil) ImgOut[i+2]=0; else ImgOut[i+2]=255;
     }

   ecrire_image_ppm(cNomImgEcrite, ImgOut,  nH, nW);
   free(ImgIn);
   return 1;
}
