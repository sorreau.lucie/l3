// histo.cpp : tableau des niveaux de gris

#include <stdio.h>
#include "image_ppm.h"
#include <iostream>
#include <fstream>

int main(int argc, char* argv[])
{
    char cNomImgLue[250], cNomImgEcrit[250];
    int nH, nW,nTaille,histo[256];
    int threshold, var_max, sum, sumB,o, q1, q2, u1, u2 = 0;
    int max_intensity = 255;
    for (int i=0;i<max_intensity;i++)histo[i]=0;

    if (argc != 2) 
     {
       printf("Usage: ImageIn.pgm \n"); 
       exit (1) ;
     }

     sscanf (argv[1],"%s",cNomImgLue) ;

        OCTET *ImgIn, *ImgOut;
     lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
        nTaille = nH * nW;
  
     allocation_tableau(ImgIn, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille);
    for(int i = 0; i< nTaille; i++){
        i = cNomImgLue[i];
        histo[i] +=1; 
    }
    for(int i=0; i<= max_intensity; i++){
        sum += i * histo[i];
    }
    for(int t = 0; t<= max_intensity; t++){
        q1 +=histo[t];
        if(q1 ==0){
            continue;
        }
        q2 = nTaille - q1;

        sumB += t*histo[t];
        u1 = sumB/q1;
        u2 = (sum - sumB)/ q2;

        o(t) = q1(t)*q2(t)*[u1(t)-u2(t)]^2

        if(o(t) > var_max){
            threshold = t;
            var_max = o(t);
        }
    }
    
    for(int i =0; i < nTaille; i++){
        if(cNomImgLue[i]> threshold){
            cNomImgEcrit[i] = 1;

        }else{
            cNomImgEcrit[i] = 0;
        }
    }
          
     ecrire_image_pgm(cNomImgEcrit, ImgOut,  nH, nW);
     free(ImgIn); free(ImgOut);

    
    
    return 1;
}