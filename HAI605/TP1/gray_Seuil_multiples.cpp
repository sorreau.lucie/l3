#include <stdio.h>
#include "image_ppm.h"

int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille;
  
  if (argc < 4) 
     {
       printf("Usage: ImageIn.pgm ImageOut.pgm S1 S2 S3 ... Sn \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgEcrite);
   
   int Sn = argc - 3;
   int S[Sn];

   printf("nbS = %d ",Sn);
   for(int i = 3; i < argc;i++){
    sscanf (argv[i],"%d",&S[i-3]);
    printf(" S%d=%d",i-2,S[i-3]); 
   }
   printf("\n");
     
   OCTET *ImgIn, *ImgOut;
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille);


 for (int i=0; i < nH; i++)
   for (int j=0; j < nW; j++)
     {
        int v = ImgIn[i*nW+j];

        int s = 0;
        for(; s < Sn;s++)
        {
          if(v < S[s]){break;}
        }
        
        ImgOut[i*nW+j] = (int)(255*(s/(float)Sn));
     }

   ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
   free(ImgIn); free(ImgOut);

   return 1;
}
