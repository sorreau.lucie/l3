// profil.cpp : profil de gris col ou ligne avec indice

#include <stdio.h>
#include "image_ppm.h"
#include <iostream>
#include <fstream>

int main(int argc, char* argv[])
{
    char cNomImgLue[250],type;
    int nH, nW,nTaille,indice;

    


    if (argc != 4) 
     {
       printf("Usage: ImageIn.pgm type num \n"); 
       exit (1) ;
     }

     sscanf (argv[1],"%s",cNomImgLue) ;
     sscanf (argv[2],"%s",&type);
     sscanf (argv[3],"%d",&indice);


     OCTET *ImgIn;
     lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
        nTaille = nH * nW;
  
        allocation_tableau(ImgIn, OCTET, nTaille);
        lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
     std::ofstream f("profile.dat");


     if (type=='c'){
      int t[nH];//tableau de taille hauteur
      for (int i=0;i<nH;i++)t[i]=0;
     int j=indice;
     for (int i=0; i < nW; i++)
         {
          t[i]=ImgIn[i*nW+j];
          f <<i<< " " <<t[i] <<"\n";
         }
        }

     else if(type=='l'){
      int t[nW];//tableau de taille largeur
      for (int i=0;i<nW;i++)t[i]=0;
        int i=indice;
        for (int j=0; j < nW; j++)
           {
               t[j]=ImgIn[i*nW+j];
          f <<j<< " " <<t[j] <<"\n";
           }
          }
          
    free(ImgIn);
    
    return 1;
}