// filtreMasqueCirculaire.cpp

#include <stdio.h>
#include <iostream>
#include <fstream>
#include "image_ppm.h"
using namespace std;

bool *masque(int size)
{
    bool *masque1 = new bool[size * size];

    for (int dx = -(size / 2); dx < (size / 2); dx++)
    {
        for (int dy = -(size / 2); dy < (size / 2); dy++)
        {
            int masquex = (size / 2) + dx;
            int masquey = (size / 2) + dy;

            int d = pow(abs(dx) + 1, 2) + pow(abs(dy) + 1, 2) - 1;
            masque1[(masquex * size) + masquey] = d <= ((size / 2) * (size / 2));
        }
    }
    return masque1;
}

int moyenne(OCTET *Img, int imgSize, int x, int y, bool *masque, int sizeMasque)
{
    int res = 0;
    int count = 0;
    for (int dx = -(sizeMasque / 2); dx < (sizeMasque / 2); dx++)
    {
        for (int dy = -(sizeMasque / 2); dy < (sizeMasque / 2); dy++)
        {
            int masquex = (sizeMasque / 2) + dx;
            int masquey = (sizeMasque / 2) + dy;

            if (masque[(masquex * sizeMasque) + masquey] == false)
            {
                continue;
            }

            int xx = x + dx;
            int yy = y + dy;

            if (xx < 0 || xx >= imgSize || yy < 0 || yy >= imgSize)
            {
                continue;
            }

            res += Img[(yy * imgSize) + xx];
            count++;
        }
    }

    return res / count;
}

void blur(char *cNomImgLue, char *cNomImgEcrite, int radius)
{
    int nH, nW, nTaille;
    OCTET *ImgIn, *ImgOut;

    lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
    nTaille = nH * nW;

    allocation_tableau(ImgIn, OCTET, nTaille);
    allocation_tableau(ImgOut, OCTET, nTaille);
    lire_image_pgm(cNomImgLue, ImgIn, nH * nW);

    bool *masque1 = masque(radius);

    for (int i = 0; i < nW; i++)
    {
        for (int j = 0; j < nH; j++)
        {
            if (i == 0 || j == 0 || i == nW - 1 || j == nH - 1)
            {
                ImgOut[j + (i * nH)] = ImgIn[j + (i * nH)];
            }
            else
            {
                ImgOut[j + (i * nH)] = moyenne(ImgIn, nH, j, i, masque1, radius);
            }
        }
    }

    ecrire_image_pgm(cNomImgEcrite, ImgOut, nH, nW);
    free(ImgIn);
    free(ImgOut);
}

int main(int argc, char *argv[])
{
    char cNomImgLue[250], cNomImgEcrite[250];
    int r;

    if (argc != 4)
    {
        printf("Usage: ImageIn.pgm  ImgOut.pgm r\n");
        exit(1);
    }

    sscanf(argv[1], "%s", cNomImgLue);
    sscanf(argv[2], "%s", cNomImgEcrite);
    sscanf(argv[3], "%d", &r);

    blur(cNomImgLue, cNomImgEcrite, r);

    return 1;
}
