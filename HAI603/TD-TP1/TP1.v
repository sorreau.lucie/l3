(* TP 1*)

(* Exo 1 *)
  (* Q1*)

Goal(forall A B : Prop, A -> B -> A).
intros.
assumption.
Qed.

  (*Q2*)
Goal (forall A B C : Prop, (A -> B -> C) -> ( A -> B) -> A -> C).
intros.
apply H.
assumption.
apply H0.
assumption.
Qed.

  (*Q3*)

Goal (forall A B C: Prop, (A -> B -> C) -> (A -> B) -> A -> C).
intros.
apply H.
assumption.
apply H0.
assumption.
Qed.

  (*Q4*)
Goal (forall A B: Prop,A /\ B ->B).
intros.
elim H.
intros.
assumption.
Qed.

  (*Q5*)
Goal( forall A B C: Prop,(A \/ B) -> (A -> C) -> (B -> C) -> C).
intros.
elim H.
- intros. apply H0. assumption.
- intros. apply H1. assumption.
Qed.

  (*Q6*)
Goal (forall A: Prop, A -> False -> ~A).
intros.
intro.
assumption.
Qed.

  (*Q7*)
Goal(forall A: Prop,False->A).
intros.
exfalso.
assumption.
Qed.

  (*Q8*)
Goal(forall A B: Prop, (A<->B) -> A -> B).
intros.
elim H.
intros.
apply H1.
assumption.
Qed.

  (*Q9*)
Goal(forall A B: Prop, (A<->B) -> B -> A).
intros.
elim H.
intros.
apply H2.
assumption.
Qed.

  (*Q10*)
Goal(forall A B: Prop, (A-> B) -> ( B->A) -> (A<->B)).
intros.
split.
intros.
apply H.
assumption.
intros.
apply H0.
assumption.
Qed.


(* Exo 2*)

    (*Q1*)
Goal(forall P Q : nat ->Prop, forall x: nat,(P x) -> exists y : nat, (P y) \/ (Q y)).
intros.
exists x.
left.
assumption.
Qed.

    (*Q2*)
Goal( forall P Q  :nat->Prop, (exists x: nat,(P x)\/(Q x))->(exists x:nat, (P x))\/(exists x:nat ,(Q x) ) ).
intros. destruct H. destruct H.
left. exists x. assumption.
right. exists x. assumption.
Qed.

    (*Q3*)
Goal(forall P Q : nat ->Prop, (forall x: nat, (P x))/\(forall x : nat, (Q x)) -> (forall x: nat,
(P x) /\ (Q x))).
intros. split. elim H. intros. apply H0.
elim H. intros. apply H1. 
Qed.

Goal( forall P Q  :nat->Prop, (forall x: nat,(P x))\/(forall x : nat ,(Q x))->(forall x:nat, (P x)/\ (Q x) )) .
intros. split. apply H. apply H.
Qed.


