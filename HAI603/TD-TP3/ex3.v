Parameter S : Set.

Inductive Fprop : Set :=
  | Symb : S -> Fprop
  | Not : Fprop -> Fprop
  | And : Fprop -> Fprop -> Fprop
  | Or : Fprop -> Fprop -> Fprop
  | Impl : Fprop -> Fprop -> Fprop
  | Equ : Fprop -> Fprop -> Fprop.

Parameter a b c : S.

Check (Symb a).

Check (Not (And (Symb a) (Symb b))).

Fixpoint nbc (f : Fprop) {struct f} : nat :=
  match f with
    | (Symb s) => 0
    | (Not s) => (plus 1 (nbc s))
    | (And a b) => (plus 1 (plus (nbc a) (nbc b)))
    | (Or a b) => (plus 1 (plus (nbc a) (nbc b)))
    | (Impl a b) => (plus 1 (plus (nbc a) (nbc b)))
    | (Equ a b) => (plus 1 (plus (nbc a) (nbc b)))
  end.

Check (nbc (Not (And (Symb a) (Symb b)))).

Fixpoint sub (f : Fprop) {struct f} : list Fprop :=
  match f with
    | (Symb s) => s::nil
    | (Not s) => (Not s)::(sub s)
    | (And a b) => (And a b)::(sub a) ++ (sub b)
    | (Or a b) => (Or a b)::(sub a) ++ (sub b)
    | (Impl a b) => (Impl a b)::(sub a) ++ (sub b)
    | (Equ a b) => (Equ a b)::(sub a) ++ (sub b)
  end.