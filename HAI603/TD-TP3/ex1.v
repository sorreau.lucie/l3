Fixpoint mult (n m: nat) {struct n} : nat :=
  match n with
  | 0 => 0
  | S p => (plus (mult p m) m)
  end.

Lemma double1: forall n:nat, mult 2 n = plus n n.
Proof.
  intros.
  simpl.
  reflexivity.
Qed.

Lemma inversePlusNul: forall x: nat, plus x 0 = plus 0 x.
Proof.
  intros.
  elim x.
  reflexivity.
  intros.
  simpl.
  rewrite H.
  simpl.
  reflexivity.
Qed.

Lemma plusS : forall m n: nat, plus m (S n) = S (plus m n).
Proof.
  intros.
  elim m.
    simpl.
    reflexivity.
  intros.
    simpl.
    rewrite <- H.
  reflexivity.
Qed.


Lemma inversePlus: forall x y:nat, plus x y = plus y x.
Proof.
  intros.
  elim x.
  elim y.
  reflexivity.
  intros.
  rewrite inversePlusNul.
  reflexivity.
  intros.
  rewrite plusS.
  rewrite <- H.
  rewrite <- plusS.
  rewrite plusS.
  reflexivity.
Qed.

Lemma SN: forall n: nat, (S n) = plus n 1.
Proof.
  intros.
  elim n.
  simpl.
  reflexivity.
  intros.
  rewrite inversePlus.
  simpl.
  reflexivity.
Qed.

Lemma Parenthese: forall n m: nat, (n + m) = n + m.
Proof.
  intros.
  reflexivity.
Qed.

Lemma double2: forall n:nat, mult n 2 = plus n n.
Proof.
  intro.
  elim n.
  simpl.
  reflexivity.
  intros.
  simpl.
  rewrite H.
  rewrite inversePlus.
  simpl.
  rewrite (inversePlus n0 (S n0)).
  simpl.
  reflexivity.
Qed.



