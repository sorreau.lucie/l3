Parameter E : Set.
Parameter P Q : E -> Prop.
Parameter a : E.


Lemma Ex1Q1 : (exists x : E, P x) /\ (forall x : E, P x -> Q x) -> (exists x : E, Q x).
Proof.
intros.
elim H.
intro.
elim H0.
intros.
exists x.
apply H2.
assumption.
Qed.


Lemma Ex1Q2 : (exists x : E, P x /\ Q x) -> (exists x : E, P x -> Q x).
Proof.
intros.
elim H.
intro.
elim H.
intros.
exists x.
intro.
elim H1.
intros.
assumption.
Qed.


(* Exercice 2 *)
  Parameter R : Set.
  Parameters zero one : R.
  Parameter opp : R -> R.
  Parameters plus mult : R -> R -> R.
  Section Commutative_ring.
  Variables a b c : R.
  Axiom ass_plus : plus (plus a b) c = plus a (plus b c).
  Axiom com_plus : plus a b = plus b a.
  Axiom com_mult : mult a b = mult b a.
  Axiom ass_mult : mult (mult a b) c = mult a ( mult b c ).
  Axiom dis_left : mult a (plus b c) = plus ( mult a b ) ( mult a c ).
  Axiom dis_right : mult (plus b c) a = plus ( mult b a ) ( mult c a ).
  Axiom neu_plus_r : plus a zero = a.
  Axiom neu_plus_l : plus zero a = a.
  Axiom neu_mult_r : mult a one = a.
  Axiom neu_mult_l : mult one a = a.
  Axiom opp_right : plus a (opp a) = zero.
  Axiom opp_left : plus (opp a) a = zero.
  End Commutative_ring.


Lemma EX2 : (forall a b : R, mult (plus a b) (plus one one) =  plus b (plus a ( plus b a))).
Proof.
intros.
rewrite dis_left.
rewrite neu_mult_r.
rewrite ass_plus.
rewrite (com_plus a0).
rewrite ass_plus.
rewrite ass_plus.
reflexivity.
Qed.



Require Export List. 
Open Scope list_scope. 
Import ListNotations .
Inductive is_length : list nat ->  nat -> Prop :=
| is_length_nil : is_length nil 0
| is_length_cons : forall (n e : nat) (l: list nat),
is_length l n -> is_length (e::l) (S n).

Fixpoint length (l : list nat) {struct l} : nat := match l with
| nil => 0
| e:: q => S (length q) 
end.

Lemma intermediaire : forall l : list nat, is_length l (length l).
  Proof.
    intro.
    induction l.

    simpl.
    apply is_length_nil.

    simpl.
    apply is_length_cons.
    assumption.
  Qed.

Lemma Ex3 : (forall l : list nat, forall n : nat, (length l) = n -> (is_length l n)).
Proof.
intros.
induction l.
rewrite <- H.
simpl.
apply is_length_nil.

rewrite <- H.
simpl.
apply is_length_cons.
apply intermediaire.
Qed.




