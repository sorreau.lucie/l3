Require Export List. 
Open Scope list_scope. 
Import ListNotations .
Inductive is_length : list nat ->  nat -> Prop :=
| is_length_nil : is_length nil 0
| is_length_cons : forall (n e : nat) (l: list nat),
is_length l n -> is_length (e::l) (S n).
						 
Fixpoint length (l : list nat) {struct l} : nat := match l with
|nil⇒0
| e::q⇒S (length q) 
end.
